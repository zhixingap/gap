top_dir=`pwd`

cd $top_dir
dir=`find . -name 'testencode.cbp' -print | sed 's:/testencode.cbp::'`
cd $dir && cbp2make -in testencode.cbp -out Makefile && make -j8

cd $top_dir
dir=`find . -name 'gapencode.cbp' -print | sed 's:/gapencode.cbp::'`
cd $dir && cbp2make -in gapencode.cbp -out Makefile && make -j8

cd $top_dir
dir=`find . -name 'gapdisplayfb.cbp' -print | sed 's:/gapdisplayfb.cbp::'`
cd $dir && cbp2make -in gapdisplayfb.cbp -out Makefile && make -j8

cd $top_dir
dir=`find . -name 'testdecode.cbp' -print | sed 's:/testdecode.cbp::'`
cd $dir && cbp2make -in testdecode.cbp -out Makefile && make -j8

cd $top_dir
dir=`find . -name 'gapcamera.cbp' -print | sed 's:/gapcamera.cbp::'`
cd $dir && cbp2make -in gapcamera.cbp -out Makefile && make -j8

cd $top_dir
dir=`find . -name 'gapdecode.cbp' -print | sed 's:/gapdecode.cbp::'`
cd $dir && cbp2make -in gapdecode.cbp -out Makefile && make -j8
