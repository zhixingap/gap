#include "ByteArray.h"

#include <math.h>
namespace zhixin{
    namespace qrcode{
ByteArray::ByteArray()
{
    //ctor
  //  bytes = null;
    size_Renamed_Field = 0;
}
ByteArray::ByteArray(int size)
{
    bytes.resize(size);
    size_Renamed_Field = size;
}
/*ByteArray::ByteArray(char* byteArray)
{
    bytes = byteArray;
    size_Renamed_Field = sizeof(bytes)/sizeof(bytes[0]);
}*/
ByteArray::~ByteArray()
{
    //dtor
    //delete[] bytes;
}
bool ByteArray::Empty(){
    return size_Renamed_Field == 0;
}
int ByteArray::at(int index)
		{
			return bytes[index] & 0xff;
		}
void  ByteArray::set_Renamed(int index, int value_Renamed)
		{
			bytes[index] = (char) value_Renamed;
		}
int ByteArray::size()
		{
			return size_Renamed_Field;
		}
void  ByteArray::appendByte(int value_Renamed)
		{
			if (size_Renamed_Field == 0 || size_Renamed_Field >= (int)(bytes.size()))
			{
				int newSize = max(32 ,size_Renamed_Field << 1);
				reserve(newSize);
			}
			bytes[size_Renamed_Field] = (char) value_Renamed;
			size_Renamed_Field++;
		}
void  ByteArray::reserve(int capacity)
		{
			if ( (int)(bytes.size())< capacity)
			{
				/*sbyte[] newArray = new sbyte[capacity];
				if (bytes != null)
				{
					Array.Copy(bytes, 0, newArray, 0, bytes.Length);
				}
				bytes = newArray;*/
				bytes.resize(capacity);
			}
		}
void  ByteArray::set_Renamed(vector<char> source, int offset, int count)
		{
			//bytes = new sbyte[count];
			bytes.clear();
			bytes.resize(count);
			size_Renamed_Field = count;
			for (int x = 0; x < count; x++)
			{
				bytes[x] = source[offset + x];
			}
		}
    }}
