#ifndef BYTEMATRIX_H
#define BYTEMATRIX_H

#include "Counted.h"
#include <string>


#include<iostream>
#include<fstream>
using namespace std;



namespace zhixin{

class ByteMatrix: public Counted
{
    public:
        ByteMatrix();
        ByteMatrix(int width, int height);
        virtual ~ByteMatrix();

        int getWidth();
        int getHeight();
     //   char[][] getArray();
        char** getArray();
        char get_Renamed(int x, int y);
        void set_Renamed(int x, int y,  char value_Renamed);
        void set_Renamed(int x, int y,  int value_Renamed);
        void clear(char value_Renamed);
        string ToString();
        void ToBitmap( unsigned char* pixels);
        void ToUnsignedCharPixel( unsigned char* pixels);
        void ToXBM( char* pixels);
        void ToCharPixel( char* pixels);
        void ToFrameBufferPixel_16( unsigned short* pixels,int w,int h);
        char unsigned2char(unsigned char src);
    protected:
    private:
        int width;
        int height;
        //char[][] bytes;
        char** bytes;
};
}
#endif // BYTEMATRIX_H
