#ifndef __VERSION_H__
#define __VERSION_H__

/*
 *  Version.h
 *  zxing
 *
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "FormatInformation.h"
#include "Counted.h"
#include "ErrorCorrectionLevel.h"
//#include <zxing/ReaderException.h>
#include "BitMatrix.h"
#include <vector>

namespace zhixin {
namespace qrcode {

class ECB :public Counted {
private:
    int count;
    int dataCodewords;
public:
    ECB(int count, int dataCodewords);
    int Count();
    int DataCodewords();
};

class ECBlocks :public Counted {
private:
    int ecCodewordsPerBlock;
    std::vector<Ref<ECB> > ecBlocks;
public:
    ECBlocks(int ecCodewordsPerBlock, Ref<ECB> ecBlocks);
    ECBlocks(int ecCodewordsPerBlock, Ref<ECB> ecBlocks1, Ref<ECB> ecBlocks2);
    int ECCodewordsPerBlock();
    std::vector<Ref<ECB> >& getECBlocks();
    int NumBlocks();
    int TotalECCodewords();


    ~ECBlocks();
};

class Version : public Counted {

private:
    int versionNumber;
    std::vector<int> &alignmentPatternCenters;
    std::vector<Ref<ECBlocks> > ecBlocks;
    int totalCodewords;
    Version(int versionNumber, std::vector<int> alignmentPatternCenters, Ref<ECBlocks> ecBlocks1, Ref<ECBlocks> ecBlocks2,
            Ref<ECBlocks> ecBlocks3, Ref<ECBlocks> ecBlocks4);

public:
    static unsigned int VERSION_DECODE_INFO[];
    static int N_VERSION_DECODE_INFOS;
    static std::vector<Ref<Version> > VERSIONS;

    ~Version();

    int VersionNumber();
    std::vector<int> &AlignmentPatternCenters();
    int TotalCodewords();
    int DimensionForVersion();
    Ref<ECBlocks> &getECBlocksForLevel(Ref<ErrorCorrectionLevel> ecLevel);
    static Ref<Version> getProvisionalVersionForDimension(int dimension);
    static Ref<Version> getVersionForNumber(int versionNumber);
    static Ref<Version> decodeVersionInformation(unsigned int versionBits);
    Ref<BitMatrix> buildFunctionPattern();
    static int buildVersions();
};
}
}

#endif // __VERSION_H__
