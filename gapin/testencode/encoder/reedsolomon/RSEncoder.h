#ifndef REEDSOLOMONENCODER_H
#define REEDSOLOMONENCODER_H

#include "GF256.h"
#include "GF256Poly.h"
#include <vector>
#include "../Counted.h"
using namespace std;
namespace zhixin {
class ReedSolomonEncoder :public Counted {
private:
    GF256 &field;
    vector< Ref<GF256Poly> > cachedGenerators;
public:

    virtual ~ReedSolomonEncoder();

    ReedSolomonEncoder(GF256 &field);
    void encode(ArrayRef<int> toEncode, int ecBytes);
    Ref<GF256Poly> buildGenerator(int degree);
protected:

};
}
#endif // REEDSOLOMONENCODER_H
