#include "RSEncoder.h"
namespace zhixin {


ReedSolomonEncoder::~ReedSolomonEncoder() {
    //dtor

}
ReedSolomonEncoder::ReedSolomonEncoder(GF256 &f)
    :Counted(), field(f) {
    /* if (!GF256.QR_CODE_FIELD.Equals(field))
    	{
    		throw new System.ArgumentException("Only QR Code is supported at this time");
    	}*/

    //this.cachedGenerators = System.Collections.ArrayList.Synchronized(new System.Collections.ArrayList(10));

    ArrayRef<int> tmp(1);
    tmp[0] = 1;


    cachedGenerators.push_back(Ref<GF256Poly>(new GF256Poly(f, tmp)) );
}
Ref<GF256Poly> ReedSolomonEncoder::buildGenerator(int degree) {

    if (degree >= (int)(cachedGenerators.size())) {
        Ref<GF256Poly> lastGenerator = Ref<GF256Poly>(cachedGenerators[cachedGenerators.size() - 1]);

        for (int d = cachedGenerators.size(); d <= degree; d++) {
            ArrayRef<int> tmp(2);
            tmp[0] = 1;
            tmp[1] = field.exp(d - 1);

            Ref<GF256Poly> gf256 = Ref<GF256Poly>(new GF256Poly(field, tmp));


            Ref<GF256Poly> nextGenerator = lastGenerator->multiply(gf256);




            cachedGenerators.push_back(nextGenerator);

            lastGenerator = nextGenerator;

        }
    }


    return cachedGenerators[degree];
}
void  ReedSolomonEncoder::encode(ArrayRef<int> toEncode, int ecBytes) {
    if (ecBytes == 0) {
        throw Exception("No error correction bytes");
    }
    int len = toEncode->size();
    int dataBytes = len- ecBytes;
    if (dataBytes <= 0) {
        throw Exception("No data bytes provided");
    }
    Ref<GF256Poly> generator = buildGenerator(ecBytes);


    //ArrayRef<int> coe = generator->Coefficients();
    //int tmp = coe->size();

    //Array.Copy(toEncode, 0, infoCoefficients, 0, dataBytes);
    ArrayRef<int> infoCoefficients(dataBytes);
    for(int i=0; i<dataBytes; i++) {
        infoCoefficients[i] = toEncode[i];
    }
    //int tmp11 = infoCoefficients.size();
    Ref<GF256Poly> info = Ref<GF256Poly>(new GF256Poly(field, infoCoefficients));
    info = info->multiplyByMonomial(ecBytes, 1);

    ArrayRef< Ref<GF256Poly> > tmp = info->divide(generator);
    Ref<GF256Poly> remainder = tmp[1];
    ArrayRef<int> coefficients = remainder->Coefficients();
    int numZeroCoefficients = ecBytes - coefficients.size();
    for (int i = 0; i < numZeroCoefficients; i++) {
        toEncode[dataBytes + i] = 0;
    }
    //Array.Copy(coefficients, 0, toEncode, dataBytes + numZeroCoefficients, coefficients.Length);
    for (int i = 0; i < int(coefficients.size()); i++) {
        toEncode[dataBytes + numZeroCoefficients+i] = coefficients[0+i];
    }
}
}
