#ifndef BITVECTOR_H
#define BITVECTOR_H

#include "Counted.h"
#include <string>
#include "Exception.h"

#include <vector>

using namespace std;


namespace zhixin {

class BitVector: public Counted {
public:
    BitVector();
    virtual ~BitVector();

    vector<char> &Array();
    int at(int index);
    void appendBit(int bit);
    void appendBits(int value_Renamed, int numBits);
    void appendBitVector(Ref<BitVector> bits);
    void xore(Ref<BitVector> other);
    string ToString();
    void  appendByte(int value_Renamed);
    int sizeInBytes();
    int size();
protected:
private:
    int sizeInBits;

    static const int DEFAULT_SIZE_IN_BYTES = 32;
    vector<char>  array;
};
}
#endif // BITVECTOR_H
