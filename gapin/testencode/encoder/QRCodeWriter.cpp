#include "QRCodeWriter.h"



namespace zhixin
{
namespace qrcode
{
QRCodeWriter::QRCodeWriter()
{
    //ctor
}
Ref<ByteMatrix> QRCodeWriter::encode(string contents, /*BarcodeFormat format,*/ int width, int height, string hints)
{

    if (/*contents == NULL ||*/ contents.length() == 0)
    {
        throw Exception("Found empty contents");
    }

    /*if (format != BarcodeFormat.BarcodeFormat_QR_CODE)
    {
    	throw Exception("Can only encode QR_CODE, but got " + format);
    }
    */
    if (width < 0 || height < 0)
    {
        throw Exception("Requested dimensions are too small: " + width + 'x' + height);
    }

    Ref<ErrorCorrectionLevel> error = Ref<ErrorCorrectionLevel>(ErrorCorrectionLevel::M);
    /*if (hints != null)
    {
    	ErrorCorrectionLevel requestedECLevel = (ErrorCorrectionLevel) hints[EncodeHintType.ERROR_CORRECTION];
    	if (requestedECLevel != null)
    	{
    		errorCorrectionLevel = requestedECLevel;
    	}
    }*/


    Ref<QRCode> code = Ref<QRCode>(new QRCode());
    Encoder::encode(contents, error, hints, code);

    return code->GetMatrix();//20130903 wangzisong  get ori 0/1
    //return renderResult(code, width, height); //get rgb 000/255255255
}

Ref<ByteMatrix> QRCodeWriter::encode(char* contents, int length,int width, int height, string hints)
{

    if (/*contents == NULL ||*/ length == 0)
    {
        throw Exception("Found empty contents");
    }

    /*if (format != BarcodeFormat.BarcodeFormat_QR_CODE)
    {
    	throw Exception("Can only encode QR_CODE, but got " + format);
    }
    */
    if (width < 0 || height < 0)
    {
        throw Exception("Requested dimensions are too small: " + width + 'x' + height);
    }

    Ref<ErrorCorrectionLevel> error = Ref<ErrorCorrectionLevel>(ErrorCorrectionLevel::M);
    /*if (hints != null)
    {
    	ErrorCorrectionLevel requestedECLevel = (ErrorCorrectionLevel) hints[EncodeHintType.ERROR_CORRECTION];
    	if (requestedECLevel != null)
    	{
    		errorCorrectionLevel = requestedECLevel;
    	}
    }*/


    Ref<QRCode> code = Ref<QRCode>(new QRCode());
    Encoder::encode(contents,length, error, hints, code);

    return code->GetMatrix();//20130903 wangzisong  get ori 0/1
    //return renderResult(code, width, height); //get rgb 000/255255255
}
QRCodeWriter::~QRCodeWriter()
{
    //dtor
}

Ref<ByteMatrix> QRCodeWriter::renderResult(Ref<QRCode> code, int width, int height)
{
    Ref<ByteMatrix> input = code->GetMatrix();
    int inputWidth = input->getWidth();
    int inputHeight = input->getHeight();

    int qrWidth = inputWidth + (QUIET_ZONE_SIZE << 1);
    int qrHeight = inputHeight + (QUIET_ZONE_SIZE << 1);
    int outputWidth = max(width, qrWidth);
    int outputHeight = max(height, qrHeight);

    int multiple = min(outputWidth / qrWidth, outputHeight / qrHeight);
    // Padding includes both the quiet zone and the extra white pixels to accommodate the requested
    // dimensions. For example, if input is 25x25 the QR will be 33x33 including the quiet zone.
    // If the requested size is 200x160, the multiple will be 4, for a QR of 132x132. These will
    // handle all the padding from 100x100 (the actual QR) up to 200x160.
    int leftPadding = (outputWidth - (inputWidth * multiple)) / 2;
    int topPadding = (outputHeight - (inputHeight * multiple)) / 2;

    Ref<ByteMatrix> output = Ref<ByteMatrix>(new ByteMatrix(outputWidth, outputHeight));
    char** outputArray = output->getArray();

    // We could be tricky and use the first row in each set of multiple as the temporary storage,
    // instead of allocating this separate array.
    char row[outputWidth];

    // 1. Write the white lines at the top
    for (int y = 0; y < topPadding; y++)
    {
        setRowColor(outputArray[y],outputWidth, (char) SupportClass::Identity(255));
    }

    // 2. Expand the QR image to the multiple
    char** inputArray = input->getArray();
    for (int y = 0; y < inputHeight; y++)
    {
        // a. Write the white pixels at the left of each row
        for (int x = 0; x < leftPadding; x++)
        {
            row[x] = (char) SupportClass::Identity(255);
        }

        // b. Write the contents of this row of the barcode
        int offset = leftPadding;
        for (int x = 0; x < inputWidth; x++)
        {
            // Redivivus.in Java to c# Porting update - Type cased sbyte
            // 30/01/2010
            // sbyte value_Renamed = (inputArray[y][x] == 1)?0:(sbyte) SupportClass.Identity(255);
            char value_Renamed = (char)((inputArray[y][x] == 1) ? 0 : SupportClass::Identity(255));
            for (int z = 0; z < multiple; z++)
            {
                row[offset + z] = value_Renamed;
            }
            offset += multiple;
        }

        // c. Write the white pixels at the right of each row
        offset = leftPadding + (inputWidth * multiple);
        for (int x = offset; x < outputWidth; x++)
        {
            row[x] = (char) SupportClass::Identity(255);
        }

        // d. Write the completed row multiple times
        offset = topPadding + (y * multiple);
        for (int z = 0; z < multiple; z++)
        {
            //Array.Copy(row, 0, outputArray[offset + z], 0, outputWidth);
            for(int i=0; i<outputWidth; i++)
                outputArray[offset + z ] [0 + i]= row[0 + i];
        }
    }

    // 3. Write the white lines at the bottom
    int offset2 = topPadding + (inputHeight * multiple);
    for (int y = offset2; y < outputHeight; y++)
    {
        setRowColor(outputArray[y], outputWidth,(char) SupportClass::Identity(255));
    }

    return output;
}

void  QRCodeWriter::setRowColor(char row[], int size, char value_Renamed)
{
    for (int x = 0; x < size; x++)
    {
        row[x] = value_Renamed;
    }
}

}
}
