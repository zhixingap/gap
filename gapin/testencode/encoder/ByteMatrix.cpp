#include "ByteMatrix.h"

using namespace std;
#include <string>
namespace zhixin
{
ByteMatrix::ByteMatrix()
{
    //ctor
}
ByteMatrix::ByteMatrix(int width, int height)
{
    bytes = new char*[height];
    for (int i = 0; i < height; i++)
    {
        bytes[i] = new char[width];
    }
    this->width = width;
    this->height = height;
}
ByteMatrix::~ByteMatrix()
{
    //dtor
    for (int i = 0; i <height; i++)
    {
        delete[] bytes[i];
    }
    delete[] bytes;
}
int ByteMatrix::getWidth()
{
    return width;
}
int ByteMatrix::getHeight()
{
    return height;
}
char** ByteMatrix::getArray()
{
    return bytes;
}
char ByteMatrix::get_Renamed(int x, int y)
{
    return bytes[y][x];
}
void  ByteMatrix::set_Renamed(int x, int y, char value_Renamed)
{
    bytes[y][x] = value_Renamed;
}

void  ByteMatrix::set_Renamed(int x, int y, int value_Renamed)
{
    bytes[y][x] = (char) value_Renamed;
}

void  ByteMatrix::clear(char value_Renamed)
{
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            bytes[y][x] = value_Renamed;
        }
    }
}
string ByteMatrix::ToString()
{
    //System.Text.StringBuilder result = new System.Text.StringBuilder(2 * width * height + 2);
    char tmp[2 * width * height + 2];
    string result(tmp);
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            switch (bytes[y][x])
            {

            case 0:
                //result.Append(" 0");
                result +=" 0";
                break;

            case 1:
                //result.Append(" 1");
                result +=" 1";
                break;

            default:
                //result.Append("  ");
                result +="  ";
                break;

            }
        }
        //result.Append('\n');
        result += '\n';
    }
    //return result.ToString();
    return result;
}

char ByteMatrix::unsigned2char(unsigned char src)
{
    return (char)(src > 127 ? src-256:src);
}
void ByteMatrix::ToUnsignedCharPixel( unsigned char* pixels)
{
    const unsigned char BLACK = 0;
    const unsigned char WHITE = 1;

    int width = this->width;
    int height = this->height;
    //unsigned char pixels[width * height];
    //int point = 0;
    for (int y = 0; y < height; y++)
    {
        //int offset = y * width;
        for (int x = 0; x < width; x++)
        {
            unsigned char res1 = bytes[y][x] == 0 ? BLACK : WHITE;

            pixels[y*width+x] = res1;
        }
    }
}

void ByteMatrix::ToCharPixel( char* pixels)
{
    char BLACK = 0;
    char WHITE = 1;

    int width = this->width;
    int height = this->height;
    //unsigned char pixels[width * height];
    //int point = 0;
    for (int y = 0; y < height; y++)
    {
        //int offset = y * width;
        for (int x = 0; x < width; x++)
        {
            pixels[y*width+x] = bytes[y][x] == 0 ? BLACK : WHITE;

        }
    }
}
void ByteMatrix::ToFrameBufferPixel_16( unsigned short* pixels,int w,int h)
{
    unsigned short BLACK = 0;
    unsigned short WHITE = 65503;

    int dw = (w-this->width)/2;
    int dh = (h-this->height)/2;

    //int width = this->width;
    //int height = this->height;
    //unsigned char pixels[width * height];
    //int point = 0;
    for (int y = 0; y < h; y++)
    {
        //int offset = y * width;
        for (int x = 0; x < w; x++)
        {
            if(y<dh||y>(h-dh-1)||x<dw||x>(w-dw-1))
            {
                pixels[y*width+x] = WHITE;
            }
            else
            {
                pixels[y*width+x] = bytes[y-dh][x-dw] == 0 ? BLACK : WHITE;
            }
        }
    }
}

void ByteMatrix::ToXBM( char* pixels)
{


    int width = this->width;
    int height = this->height;

    //init dst data to 0
    int initLen = width*height/8;
    for(int i=0; i<initLen; i++)
    {
        pixels[i]=0;
    }

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            int pos = y*width+x;
            //if(bytes[y][x]==0)
            //rotate to left 90  by wangzisong
            if(bytes[x][height-y]==0)
            {
                //curr_p->data[i/8]|=(1<<(7-i%8)) ;
                pixels[pos/8]|=(1<<(pos%8)) ;
            }

        }
    }
}

void ByteMatrix::ToBitmap( unsigned char* pixels)
{
    const unsigned char BLACK = 0;
    const unsigned char WHITE = 255;
    char** array = this->bytes;
    int width = this->width;
    int height = this->height;
    //unsigned char pixels[width * height];
    int point = 0;
    for (int y = 0; y < height; y++)
    {
        //int offset = y * width;
        for (int x = 0; x < width; x++)
        {
            unsigned char res1 = array[y][x] == 0 ? BLACK : WHITE;
            /* if(array[y][x] == 0){
                 int i = 0;
             }*/
            // char res2 = unsigned2char(res1);
            pixels[point] = res1;
            pixels[point + 1] = res1;
            pixels[point + 2] = res1;
            point += 3;
        }
    }


    /*
    unsigned char hBitMapInfo[sizeof(BITMAPINFOHEADER)+sizeof(RGBQUAD)* 256];
            BITMAPINFO *bmp_info=(tagBITMAPINFO *) hBitMapInfo;
    BITMAPFILEHEADER bmp_file_header;
    BITMAPFILEHEADER*   p_bmp_file_header=&bmp_file_header;

    for( int i=0;i<256;i++)
    {
    bmp_info->bmiColors[i].rgbBlue=i;
    bmp_info->bmiColors[i].rgbGreen=i;
    bmp_info->bmiColors[i].rgbRed=i;
    bmp_info->bmiColors[i].rgbReserved=0;
    }
    bmp_info->bmiHeader.biSize =sizeof(BITMAPINFOHEADER);
    bmp_info->bmiHeader.biWidth =width;
    bmp_info->bmiHeader.biHeight  = height;//根据实际情况，DIB or DDB ，故可能是-matrix.n_row
    bmp_info->bmiHeader.biPlanes =1;
    bmp_info->bmiHeader.biBitCount =8;
    bmp_info->bmiHeader.biCompression =BI_RGB;
    bmp_info->bmiHeader.biSizeImage =0;
    bmp_info->bmiHeader.biXPelsPerMeter =0;
    bmp_info->bmiHeader.biYPelsPerMeter =0;
    bmp_info->bmiHeader.biClrImportant =0;
    bmp_info->bmiHeader.biClrUsed =0;

    bmp_file_header.bfType = ((WORD)('M'<<8)|'B');
    bmp_file_header.bfReserved1= 0;
    bmp_file_header.bfReserved2= 0;
    bmp_file_header.bfOffBits=       (DWORD	(	sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+
                                             256*sizeof(RGBQUAD)	));
    bmp_file_header.bfSize          =(DWORD)(bmp_file_header.bfOffBits+(DWORD)(width*height));

    ofstream outFile;
    string bmpname = "E:\\test.bmp";

    outFile.open(bmpname.c_str(),ios::out|ios::binary|ios::app);
    outFile.write((const char*)p_bmp_file_header,sizeof(BITMAPFILEHEADER));
    outFile.write((const char*)bmp_info,1064);
    outFile.write((const char*)pixels,width*height);
    outFile.clear();
    outFile.close();


        */
}
}
