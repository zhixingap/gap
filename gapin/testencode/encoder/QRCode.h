#ifndef QRCODE_H
#define QRCODE_H

using namespace std;
#include <string>

#include "Counted.h"
#include "Mode.h"
#include "ByteMatrix.h"
#include "ErrorCorrectionLevel.h"

namespace zhixin {

class QRCode: public Counted {
public:
    QRCode();
    virtual ~QRCode();

    bool getValid();
    Ref<ByteMatrix> GetMatrix();
    void setMatrix(Ref<ByteMatrix> matrix);

    static  const int NUM_MASK_PATTERNS = 8;
    int at(int x, int y);
    //string ToString();
    static bool isValidMaskPattern(int maskPattern);

    int version;
    int matrixWidth;
    int maskPattern;
    int numTotalBytes;
    int numDataBytes;
    int numECBytes;
    int numRSBlocks;
    Ref<zhixin::qrcode::Mode> mode;
    Ref<zhixin::qrcode::ErrorCorrectionLevel> ecLevel;


protected:
private:
    Ref<ByteMatrix> matrix;



};
}
#endif // QRCODE_H
