#ifndef MATRIXUTIL_H
#define MATRIXUTIL_H

#include "Counted.h"
#include "BitVector.h"
#include "ByteMatrix.h"
#include "ErrorCorrectionLevel.h"
#include "MaskUtil.h"
#include "SupportClass.h"
#include "QRCode.h"


namespace zhixin{
    namespace qrcode{

class MatrixUtil : public Counted
{
    public:
        MatrixUtil();
        virtual ~MatrixUtil();



        static void  embedDataBits(Ref<BitVector> dataBits, int maskPattern, Ref<ByteMatrix> matrix);
        static void maybeEmbedVersionInfo(int version, Ref<ByteMatrix> matrix);
        static void  embedBasicPatterns(int version, Ref<ByteMatrix> matrix);
        static void  clearMatrix(Ref<ByteMatrix> matrix);
        static void  buildMatrix(Ref<BitVector> dataBits, Ref<ErrorCorrectionLevel> ecLevel, int version, int maskPattern, Ref<ByteMatrix> matrix);
        static void  embedTypeInfo(Ref<ErrorCorrectionLevel> ecLevel, int maskPattern, Ref<ByteMatrix> matrix);
        static int findMSBSet(int value_Renamed);
        static int calculateBCHCode(int value_Renamed, int poly);
        static void makeTypeInfoBits(Ref<ErrorCorrectionLevel> ecLevel, int maskPattern, Ref<BitVector> bits);
        static void makeVersionInfoBits(int version, Ref<BitVector> bits);
   static bool isEmpty(int value_Renamed);
   static bool isValidValue(int value_Renamed);
   static void embedTimingPatterns(Ref<ByteMatrix> matrix);
   static void embedDarkDotAtLeftBottomCorner(Ref<ByteMatrix> matrix);
   static void embedHorizontalSeparationPattern(int xStart, int yStart, Ref<ByteMatrix> matrix);
   static void embedVerticalSeparationPattern(int xStart, int yStart, Ref<ByteMatrix> matrix);
   static void embedPositionAdjustmentPattern(int xStart, int yStart, Ref<ByteMatrix> matrix);
   static void embedPositionDetectionPattern(int xStart, int yStart, Ref<ByteMatrix> matrix);
   static void embedPositionDetectionPatternsAndSeparators(Ref<ByteMatrix> matrix);
   static void maybeEmbedPositionAdjustmentPatterns(int version, Ref<ByteMatrix> matrix);



    protected:
    private:
        static  int POSITION_DETECTION_PATTERN[7][7];
		//UPGRADE_NOTE: Final was removed from the declaration of 'HORIZONTAL_SEPARATION_PATTERN'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
		static  int HORIZONTAL_SEPARATION_PATTERN[1][8];
		//UPGRADE_NOTE: Final was removed from the declaration of 'VERTICAL_SEPARATION_PATTERN'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
		static  int VERTICAL_SEPARATION_PATTERN[7][1];
		//UPGRADE_NOTE: Final was removed from the declaration of 'POSITION_ADJUSTMENT_PATTERN'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
		static  int POSITION_ADJUSTMENT_PATTERN[5][5];
		// From Appendix E. Table 1, JIS0510X:2004 (p 71). The table was double-checked by komatsu.
		//UPGRADE_NOTE: Final was removed from the declaration of 'POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
		static  int POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[40][7];
		// Type info cells at the left top corner.
		//UPGRADE_NOTE: Final was removed from the declaration of 'TYPE_INFO_COORDINATES'. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1003'"
		static  int TYPE_INFO_COORDINATES[15][2];
		// From Appendix D in JISX0510:2004 (p. 67)
		static const int VERSION_INFO_POLY = 0x1f25; // 1 1111 0010 0101

		// From Appendix C in JISX0510:2004 (p.65).
		static const int TYPE_INFO_POLY = 0x537;
		static const int TYPE_INFO_MASK_PATTERN = 0x5412;
};
    }}
#endif // MATRIXUTIL_H
