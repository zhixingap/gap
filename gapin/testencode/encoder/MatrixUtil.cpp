#include "MatrixUtil.h"
namespace zhixin {
namespace qrcode {
MatrixUtil::MatrixUtil() {
    //ctor
}

MatrixUtil::~MatrixUtil() {
    //dtor
}

int MatrixUtil::POSITION_DETECTION_PATTERN[7][7] = {{1, 1, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 0, 0, 1}, {1, 0, 1, 1, 1, 0, 1}, {1, 0, 1, 1, 1, 0, 1}, {1, 0, 1, 1, 1, 0, 1}, {1, 0, 0, 0, 0, 0, 1}, {1, 1, 1, 1, 1, 1, 1}};
int MatrixUtil::HORIZONTAL_SEPARATION_PATTERN[1][8] = {{0, 0, 0, 0, 0, 0, 0, 0}};
int MatrixUtil::VERTICAL_SEPARATION_PATTERN[7][1] = {{0},{0},{0}, {0}, {0}, {0}, {0}};
int MatrixUtil::POSITION_ADJUSTMENT_PATTERN[5][5] = {{1, 1, 1, 1, 1},{1, 0, 0, 0, 1}, {1, 0, 1, 0, 1}, {1, 0, 0, 0, 1}, {1, 1, 1, 1, 1}};
int MatrixUtil::POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[40][7] = {{- 1, - 1, - 1, - 1, - 1, - 1, - 1}, {6, 18, - 1, - 1, - 1, - 1, - 1}, {6, 22, - 1, - 1, - 1, - 1, - 1}, {6, 26, - 1, - 1, - 1, - 1, - 1}, {6, 30, - 1, - 1, - 1, - 1, - 1}, {6, 34, - 1, - 1, - 1, - 1, - 1}, {6, 22, 38, - 1, - 1, - 1, - 1}, {6, 24, 42, - 1, - 1, - 1, - 1}, {6, 26, 46, - 1, - 1, - 1, - 1}, {6, 28, 50, - 1, - 1, - 1, - 1}, {6, 30, 54, - 1, - 1, - 1, - 1}, {6, 32, 58, - 1, - 1, - 1, - 1}, {6, 34, 62, - 1, - 1, - 1, - 1}, {6, 26, 46, 66, - 1, - 1, - 1}, {6, 26, 48, 70, - 1, - 1, - 1}, {6, 26, 50, 74, - 1, - 1, - 1},{6, 30, 54, 78, - 1, - 1, - 1}, {6, 30, 56, 82, - 1, - 1, - 1}, {6, 30, 58, 86, - 1, - 1, - 1}, {6, 34, 62, 90, - 1, - 1, - 1}, {6, 28, 50, 72, 94, - 1, - 1}, {6, 26, 50, 74, 98, - 1, - 1},
    {6, 30, 54, 78, 102, - 1, - 1}, {6, 28, 54, 80, 106, - 1, - 1},{6, 32, 58, 84, 110, - 1, - 1}, {6, 30, 58, 86, 114, - 1, - 1}, {6, 34, 62, 90, 118, - 1, - 1}, {6, 26, 50, 74, 98, 122, - 1},{6, 30, 54, 78, 102, 126, - 1}, {6, 26, 52, 78, 104, 130, - 1}, {6, 30, 56, 82, 108, 134, - 1}, {6, 34, 60, 86, 112, 138, - 1}, {6, 30, 58, 86, 114, 142, - 1}, {6, 34, 62, 90, 118, 146, - 1}, {6, 30, 54, 78, 102, 126, 150},{6, 24, 50, 76, 102, 128, 154}, {6, 28, 54, 80, 106, 132, 158}, {6, 32, 58, 84, 110, 136, 162}, {6, 26, 54, 82, 110, 138, 166},{6, 30, 58, 86, 114, 142, 170}
};
int MatrixUtil::TYPE_INFO_COORDINATES[15][2] = {{8, 0}, {8, 1}, {8, 2}, {8, 3}, {8, 4}, {8, 5}, {8, 7}, {8, 8}, {7, 8}, {5, 8}, {4, 8}, {3, 8},
    {2, 8}, {1, 8}, {0, 8}
};


void MatrixUtil::clearMatrix(Ref<ByteMatrix> matrix) {
    matrix->clear((char) (- 1));
}

void  MatrixUtil::buildMatrix(Ref<BitVector> dataBits, Ref<ErrorCorrectionLevel> ecLevel, int version, int maskPattern, Ref<ByteMatrix> matrix) {
    clearMatrix(matrix);
    embedBasicPatterns(version, matrix);
    // Type information appear with any version.
    embedTypeInfo(ecLevel, maskPattern, matrix);
    // Version info appear if version >= 7.
    maybeEmbedVersionInfo(version, matrix);
    // Data should be embedded at end.
    embedDataBits(dataBits, maskPattern, matrix);
}
void  MatrixUtil::embedBasicPatterns(int version, Ref<ByteMatrix> matrix) {
    // Let's get started with embedding big squares at corners.
    embedPositionDetectionPatternsAndSeparators(matrix);
    // Then, embed the dark dot at the left bottom corner.
    embedDarkDotAtLeftBottomCorner(matrix);

    // Position adjustment patterns appear if version >= 2.
    maybeEmbedPositionAdjustmentPatterns(version, matrix);
    // Timing patterns should be embedded after position adj. patterns.
    embedTimingPatterns(matrix);
}

void  MatrixUtil::embedTypeInfo(Ref<ErrorCorrectionLevel> ecLevel, int maskPattern, Ref<ByteMatrix> matrix) {
    Ref<BitVector> typeInfoBits = Ref<BitVector>(new BitVector());
    makeTypeInfoBits(ecLevel, maskPattern, typeInfoBits);

    for (int i = 0; i < typeInfoBits->size(); ++i) {
        // Place bits in LSB to MSB order.  LSB (least significant bit) is the last value in
        // "typeInfoBits".
        int bit = typeInfoBits->at(typeInfoBits->size() - 1 - i);

        // Type info bits at the left top corner. See 8.9 of JISX0510:2004 (p.46).
        int x1 = TYPE_INFO_COORDINATES[i][0];
        int y1 = TYPE_INFO_COORDINATES[i][1];
        matrix->set_Renamed(x1, y1, bit);

        if (i < 8) {
            // Right top corner.
            int x2 = matrix->getWidth() - i - 1;
            int y2 = 8;
            matrix->set_Renamed(x2, y2, bit);
        } else {
            // Left bottom corner.
            int x2 = 8;
            int y2 = matrix->getHeight() - 7 + (i - 8);
            matrix->set_Renamed(x2, y2, bit);
        }
    }
}

void  MatrixUtil::maybeEmbedVersionInfo(int version, Ref<ByteMatrix> matrix) {
    if (version < 7) {
        // Version info is necessary if version >= 7.
        return ; // Don't need version info.
    }
    Ref<BitVector> versionInfoBits = Ref<BitVector>(new BitVector());
    makeVersionInfoBits(version, versionInfoBits);

    int bitIndex = 6 * 3 - 1; // It will decrease from 17 to 0.
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 3; ++j) {
            // Place bits in LSB (least significant bit) to MSB order.
            int bit = versionInfoBits->at(bitIndex);
            bitIndex--;
            // Left bottom corner.
            matrix->set_Renamed(i, matrix->getHeight() - 11 + j, bit);
            // Right bottom corner.
            matrix->set_Renamed(matrix->getHeight() - 11 + j, i, bit);
        }
    }
}
void  MatrixUtil::embedDataBits(Ref<BitVector> dataBits, int maskPattern, Ref<ByteMatrix> matrix) {
    int bitIndex = 0;
    int direction = - 1;
    // Start from the right bottom cell.
    int x = matrix->getWidth() - 1;
    int y = matrix->getHeight() - 1;
    while (x > 0) {
        // Skip the vertical timing pattern.
        if (x == 6) {
            x -= 1;
        }
        while (y >= 0 && y < matrix->getHeight()) {
            for (int i = 0; i < 2; ++i) {
                int xx = x - i;
                // Skip the cell if it's not empty.
                if (!isEmpty(matrix->get_Renamed(xx, y))) {
                    continue;
                }
                int bit;
                if (bitIndex < dataBits->size()) {
                    bit = dataBits->at(bitIndex);
                    ++bitIndex;
                } else {
                    // Padding bit. If there is no bit left, we'll fill the left cells with 0, as described
                    // in 8.4.9 of JISX0510:2004 (p. 24).
                    bit = 0;
                }

                // Skip masking if mask_pattern is -1.
                if (maskPattern != - 1) {
                    if (MaskUtil::getDataMaskBit(maskPattern, xx, y)) {
                        bit ^= 0x1;
                    }
                }
                matrix->set_Renamed(xx, y, bit);
            }
            y += direction;
        }
        direction = - direction; // Reverse the direction.
        y += direction;
        x -= 2; // Move to the left.
    }
    // All bits should be consumed.
    if (bitIndex != dataBits->size()) {
        throw Exception("Not all bits consumed: " + bitIndex + '/' + dataBits->size());
    }
}
int MatrixUtil::findMSBSet(int value_Renamed) {
    int numDigits = 0;
    while (value_Renamed != 0) {
        value_Renamed = SupportClass::URShift(value_Renamed, 1);
        ++numDigits;
    }
    return numDigits;
}
int MatrixUtil::calculateBCHCode(int value_Renamed, int poly) {
    // If poly is "1 1111 0010 0101" (version info poly), msbSetInPoly is 13. We'll subtract 1
    // from 13 to make it 12.
    int msbSetInPoly = findMSBSet(poly);
    value_Renamed <<= msbSetInPoly - 1;
    // Do the division business using exclusive-or operations.
    while (findMSBSet(value_Renamed) >= msbSetInPoly) {
        value_Renamed ^= poly << (findMSBSet(value_Renamed) - msbSetInPoly);
    }
    // Now the "value" is the remainder (i.e. the BCH code)
    return value_Renamed;
}
void  MatrixUtil::makeTypeInfoBits(Ref<ErrorCorrectionLevel> ecLevel, int maskPattern, Ref<BitVector> bits) {
    if (!QRCode::isValidMaskPattern(maskPattern)) {
        throw Exception("Invalid mask pattern");
    }
    int typeInfo = (ecLevel->bits() << 3) | maskPattern;
    bits->appendBits(typeInfo, 5);

    int bchCode = calculateBCHCode(typeInfo, TYPE_INFO_POLY);
    bits->appendBits(bchCode, 10);

    Ref<BitVector> maskBits = Ref<BitVector>(new BitVector());
    maskBits->appendBits(TYPE_INFO_MASK_PATTERN, 15);
    bits->xore(maskBits);

    if (bits->size() != 15) {
        // Just in case.
        throw Exception("should not happen but we got: " + bits->size());
    }
}
void  MatrixUtil::makeVersionInfoBits(int version, Ref<BitVector> bits) {
    bits->appendBits(version, 6);
    int bchCode = calculateBCHCode(version, VERSION_INFO_POLY);
    bits->appendBits(bchCode, 12);

    if (bits->size() != 18) {
        // Just in case.
        throw Exception("should not happen but we got: " + bits->size());
    }
}
bool MatrixUtil::isEmpty(int value_Renamed) {
    return value_Renamed == - 1;
}
bool MatrixUtil::isValidValue(int value_Renamed) {
    return (value_Renamed == - 1 || value_Renamed == 0 || value_Renamed == 1); // Dark (black).
}
void  MatrixUtil::embedTimingPatterns(Ref<ByteMatrix> matrix) {
    // -8 is for skipping position detection patterns (size 7), and two horizontal/vertical
    // separation patterns (size 1). Thus, 8 = 7 + 1.
    for (int i = 8; i < matrix->getWidth() - 8; ++i) {
        int bit = (i + 1) % 2;
        // Horizontal line.
        if (!isValidValue(matrix->get_Renamed(i, 6))) {
            throw Exception("valid value");
        }
        if (isEmpty(matrix->get_Renamed(i, 6))) {
            matrix->set_Renamed(i, 6, bit);
        }
        // Vertical line.
        if (!isValidValue(matrix->get_Renamed(6, i))) {
            throw Exception("valid value");
        }
        if (isEmpty(matrix->get_Renamed(6, i))) {
            matrix->set_Renamed(6, i, bit);
        }
    }
}
void  MatrixUtil::embedDarkDotAtLeftBottomCorner(Ref<ByteMatrix> matrix) {
    if (matrix->get_Renamed(8, matrix->getHeight() - 8) == 0) {
        throw Exception("valid value");
    }
    matrix->set_Renamed(8, matrix->getHeight() - 8, 1);
}
void  MatrixUtil::embedHorizontalSeparationPattern(int xStart, int yStart, Ref<ByteMatrix> matrix) {
    // We know the width and height.
    /*if (sizeof(HORIZONTAL_SEPARATION_PATTERN[0]) != 8 || sizeof(HORIZONTAL_SEPARATION_PATTERN) != 1)
    {
    	throw Exception("Bad horizontal separation pattern");
    }*/
    for (int x = 0; x < 8; ++x) {
        if (!isEmpty(matrix->get_Renamed(xStart + x, yStart))) {
            throw Exception("valid value");
        }
        matrix->set_Renamed(xStart + x, yStart, HORIZONTAL_SEPARATION_PATTERN[0][x]);
    }
}
void  MatrixUtil::embedVerticalSeparationPattern(int xStart, int yStart, Ref<ByteMatrix> matrix) {
    // We know the width and height.
    /*	if (sizeof(VERTICAL_SEPARATION_PATTERN[0]) != 1 || sizeof(VERTICAL_SEPARATION_PATTERN) != 7)
    	{
    		throw Exception("Bad vertical separation pattern");
    	}*/
    for (int y = 0; y < 7; ++y) {
        if (!isEmpty(matrix->get_Renamed(xStart, yStart + y))) {
            throw Exception("valid value");
        }
        matrix->set_Renamed(xStart, yStart + y, VERTICAL_SEPARATION_PATTERN[y][0]);
    }
}
void  MatrixUtil::embedPositionAdjustmentPattern(int xStart, int yStart, Ref<ByteMatrix> matrix) {
    // We know the width and height.
    /*	if (sizeof(POSITION_ADJUSTMENT_PATTERN[0]) != 5 || sizeof(POSITION_ADJUSTMENT_PATTERN) != 5)
    	{
    		throw Exception("Bad position adjustment");
    	}*/
    for (int y = 0; y < 5; ++y) {
        for (int x = 0; x < 5; ++x) {
            if (!isEmpty(matrix->get_Renamed(xStart + x, yStart + y))) {
                throw Exception("valid value");
            }
            matrix->set_Renamed(xStart + x, yStart + y, POSITION_ADJUSTMENT_PATTERN[y][x]);
        }
    }
}
void  MatrixUtil::embedPositionDetectionPattern(int xStart, int yStart, Ref<ByteMatrix> matrix) {
    // We know the width and height.
    /*			if (sizeof(POSITION_DETECTION_PATTERN[0]) != 7 || sizeof(POSITION_DETECTION_PATTERN) != 7)
    			{
    				throw Exception("Bad position detection pattern");
    			}*/
    for (int y = 0; y < 7; ++y) {
        for (int x = 0; x < 7; ++x) {
            int tt = matrix->get_Renamed(xStart + x, yStart + y);
            if (!isEmpty(tt)) {
                throw Exception("valid value");
            }
            matrix->set_Renamed(xStart + x, yStart + y, POSITION_DETECTION_PATTERN[y][x]);
        }
    }
}
void  MatrixUtil::embedPositionDetectionPatternsAndSeparators(Ref<ByteMatrix> matrix) {
    // Embed three big squares at corners.
    int pdpWidth = 7;//sizeof(POSITION_DETECTION_PATTERN[0]);
    // Left top corner.
    embedPositionDetectionPattern(0, 0, matrix);
    // Right top corner.
    embedPositionDetectionPattern(matrix->getWidth() - pdpWidth, 0, matrix);
    // Left bottom corner.
    embedPositionDetectionPattern(0, matrix->getWidth() - pdpWidth, matrix);

    // Embed horizontal separation patterns around the squares.
    int hspWidth = 8;//sizeof(HORIZONTAL_SEPARATION_PATTERN[0]);
    // Left top corner.
    embedHorizontalSeparationPattern(0, hspWidth - 1, matrix);
    // Right top corner.
    embedHorizontalSeparationPattern(matrix->getWidth() - hspWidth, hspWidth - 1, matrix);
    // Left bottom corner.
    embedHorizontalSeparationPattern(0, matrix->getWidth() - hspWidth, matrix);

    // Embed vertical separation patterns around the squares.
    int vspSize = 7;//sizeof(VERTICAL_SEPARATION_PATTERN);
    // Left top corner.
    embedVerticalSeparationPattern(vspSize, 0, matrix);
    // Right top corner.
    embedVerticalSeparationPattern(matrix->getHeight() - vspSize - 1, 0, matrix);
    // Left bottom corner.
    embedVerticalSeparationPattern(vspSize, matrix->getHeight() - vspSize, matrix);
}
void  MatrixUtil::maybeEmbedPositionAdjustmentPatterns(int version, Ref<ByteMatrix> matrix) {
    if (version < 2) {
        // The patterns appear if version >= 2
        return ;
    }
    int index = version - 1;
    //int coordinates[] = POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[index];
    int numCoordinates = 7;//sizeof(POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[index]);
    for (int i = 0; i < numCoordinates; ++i) {
        for (int j = 0; j < numCoordinates; ++j) {
            int y = POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[index][i];
            int x = POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[index][j];
            if (x == - 1 || y == - 1) {
                continue;
            }
            // If the cell is unset, we embed the position adjustment pattern here.
            if (isEmpty(matrix->get_Renamed(x, y))) {
                // -2 is necessary since the x/y coordinates point to the center of the pattern, not the
                // left top corner.
                embedPositionAdjustmentPattern(x - 2, y - 2, matrix);
            }
        }
    }
}

}
}
