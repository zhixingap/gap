#ifndef ENCODER_H
#define ENCODER_H

using namespace std;
using namespace std;
#include <string>
#include <math.h>

#include "Counted.h"
#include "CharacterSetECI.h"
#include "BitVector.h"
#include "ErrorCorrectionLevel.h"
#include "SupportClass.h"
#include "QRCode.h"
#include "MatrixUtil.h"
#include "MaskUtil.h"
#include "BlockPair.h"
#include "reedsolomon/Array.h"
#include "reedsolomon/RSEncoder.h"
#include "CharacterSetECI.h"
#include <iconv.h>
#include <stdlib.h>
#include <stdio.h>

namespace zhixin
{
namespace qrcode
{
class Encoder
{
public:
    Encoder();
    virtual ~Encoder();

    static string DEFAULT_BYTE_MODE_ENCODING;
    // static void  encode(string content, Ref<ErrorCorrectionLevel> ecLevel, Ref<QRCode> qrCode);
    //using
    static void  encode(string content, Ref<ErrorCorrectionLevel> ecLevel, string hints, Ref<QRCode> qrCode);
    static void  encode( char* pcontent, int length,Ref<ErrorCorrectionLevel> ecLevel, string hints, Ref<QRCode> qrCode);//licheng add
    static int  getAlphanumericCode(int code);
    static Ref<Mode> chooseMode(string content);
    static Ref<Mode> chooseMode(string content, string encoding);
    static void initQRCode(int numInputBytes, Ref<ErrorCorrectionLevel> ecLevel,Ref<Mode> mode, Ref<QRCode> qrCode);
    static void  terminateBits(int numDataBytes, Ref<BitVector> bits);
    static void  getNumDataBytesAndNumECBytesForBlockID(int numTotalBytes, int numDataBytes, int numRSBlocks, int blockID, int numDataBytesInBlock[], int numECBytesInBlock[]);
    static void  interleaveWithECBytes(Ref<BitVector> bits, int numTotalBytes, int numDataBytes, int numRSBlocks, Ref<BitVector> result);
    static void  appendModeInfo(Ref<Mode> mode, Ref<BitVector> bits);
    static void  appendLengthInfo(int numLetters, int version, Ref<Mode> mode, Ref<BitVector> bits);
    static void  appendBytes(string content, Ref<Mode> mode, Ref<BitVector> bits, string encoding);
    static void  appendNumericBytes(string content, Ref<BitVector> bits);
    static void  appendAlphanumericBytes(string content, Ref<BitVector> bits);
    static void  append8BitBytes(string content, Ref<BitVector> bits, string encoding);
    static void  appendECI(common::CharacterSetECI* eci, Ref<BitVector> bits);
    static int calculateMaskPenalty(Ref<ByteMatrix> matrix);
    static bool isOnlyDoubleByteKanji(string content);
    static int chooseMaskPattern(Ref<BitVector> bits, Ref<ErrorCorrectionLevel> ecLevel, int version, Ref<ByteMatrix> matrix);
    static Ref<ByteArray> generateECBytes(Ref<ByteArray> dataBytes, int numEcBytesInBlock);

    static int ALPHANUMERIC_TABLE[96] ;

    static int convert(char *out, size_t outlen, char* in, size_t inlen, const char *tocode, const char* fromcode);


protected:
private:


};
}
}
#endif // ENCODER_H
