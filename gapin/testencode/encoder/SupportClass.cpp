#include "SupportClass.h"
#include <string.h>
namespace zhixin {
namespace qrcode {
SupportClass::SupportClass() {
    //ctor
}

SupportClass::~SupportClass() {
    //dtor
}
/*
unsigned char* SupportClass::ToByteArray(char sbyteArray[])
	{
unsigned char* byteArray;
		if (sbyteArray != NULL)
		{
			 byteArray = new unsigned char[sizeof(sbyteArray)];
			for(unsigned int index=0; index < sizeof(sbyteArray); index++)
				byteArray[index] = (unsigned char) sbyteArray[index];

		}
		return byteArray;
	}
char* SupportClass::ToByteArray(string sourceString)
	{
		//return System.Text.UTF8Encoding.UTF8.GetBytes(sourceString);
		return NULL;
	}
public static byte[] ToByteArray(System.Object[] tempObjectArray)
	{
		byte[] byteArray = null;
		if (tempObjectArray != null)
		{
			byteArray = new byte[tempObjectArray.Length];
			for (int index = 0; index < tempObjectArray.Length; index++)
				byteArray[index] = (byte)tempObjectArray[index];
		}
		return byteArray;
	}*/
int SupportClass::URShift(int number, int bits) {
    if ( number >= 0)
        return number >> bits;
    else
        return (number >> bits) + (2 << ~bits);
}
/*
int SupportClass::URShift(int number, long bits)
	{
		return URShift(number, (int)bits);
	}
long SupportClass::URShift(long number, int bits)
	{
		if ( number >= 0)
			return number >> bits;
		else
			return (number >> bits) + (2L << ~bits);
	}
long SupportClass::URShift(long number, long bits)
	{
		return URShift(number, (int)bits);
	}
long SupportClass::Identity(long literal)
	{
		return literal;
	}
unsigned long SupportClass::Identity(unsigned long literal)
	{
		return literal;
	}
float SupportClass::Identity(float literal)
	{
		return literal;
	}*/
int SupportClass::Identity(int literal) {
    return literal;
}
/*
void SupportClass::GetCharsFromString(string sourceString, int sourceStart, int sourceEnd, char destinationArray[], int destinationStart)
	{
		int sourceCounter;
		int destinationCounter;
		sourceCounter = sourceStart;
		destinationCounter = destinationStart;
		while (sourceCounter < sourceEnd)
		{
			destinationArray[destinationCounter] = (char) sourceString[sourceCounter];
			sourceCounter++;
			destinationCounter++;
		}
	}*/
/*public static void SetCapacity(System.Collections.ArrayList vector, int newCapacity)
	{
		if (newCapacity > vector.Count)
			vector.AddRange(new Array[newCapacity-vector.Count]);
		else if (newCapacity < vector.Count)
			vector.RemoveRange(newCapacity, vector.Count - newCapacity);
		vector.Capacity = newCapacity;
	}*/
ArrayRef<char> SupportClass::ToSByteArray(ArrayRef<unsigned char> byteArray) {
    int len = byteArray->size();
    ArrayRef<char> sbyteArray(len);
    //if (byteArray != NULL)
    if(len>0) {
        for(int index=0; index < len; index++)
            sbyteArray[index] = (char) byteArray[index];
    }
    return sbyteArray;
}
}
}
