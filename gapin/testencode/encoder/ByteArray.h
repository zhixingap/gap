#ifndef BYTEARRAY_H
#define BYTEARRAY_H


#include "Counted.h"
#include <vector>
using namespace std;

namespace zhixin {
namespace qrcode {
class ByteArray: public Counted {
public:
    ByteArray();
    ByteArray(int size);
    //ByteArray(char* byteArray);
    virtual ~ByteArray();

    bool Empty();
    int at(int index);
    void set_Renamed(int index, int value_Renamed);
    int size();
    void appendByte(int value_Renamed);
    void  reserve(int capacity);
    void set_Renamed(vector<char> source, int offset, int count);




protected:
private:
    int size_Renamed_Field;
    vector<char> bytes;
    static const int INITIAL_SIZE = 32;
};
}
}
#endif // BYTEARRAY_H
