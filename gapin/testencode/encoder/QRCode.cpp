#include "QRCode.h"
namespace zhixin {

QRCode::QRCode() {
    mode = NULL;
    ecLevel = NULL;
    version = - 1;
    matrixWidth = - 1;
    maskPattern = - 1;
    numTotalBytes = - 1;
    numDataBytes = - 1;
    numECBytes = - 1;
    numRSBlocks = - 1;
    matrix = NULL;
}

QRCode::~QRCode() {
    //dtor
}

Ref<ByteMatrix> QRCode::GetMatrix() {
    return Ref<ByteMatrix>(matrix);
}
void QRCode::setMatrix(Ref<ByteMatrix> matrix) {
    this->matrix = matrix;
}
bool QRCode::getValid() {
    return mode != NULL && ecLevel != NULL && version != - 1 && matrixWidth != - 1 && maskPattern != - 1 && numTotalBytes != - 1 && numDataBytes != - 1 && numECBytes != - 1 && numRSBlocks != - 1 && isValidMaskPattern(maskPattern) && numTotalBytes == numDataBytes + numECBytes && matrix != NULL && matrixWidth == matrix->getWidth() && matrix->getWidth() == matrix->getHeight(); // Must be square.
}
int QRCode::at(int x, int y) {
    // The value must be zero or one.
    int value_Renamed = matrix->get_Renamed(x, y);
    if (!(value_Renamed == 0 || value_Renamed == 1)) {
        // this is really like an assert... not sure what better exception to use?
        throw Exception("Bad value");
    }
    return value_Renamed;
}
/*
string QRCode::ToString()
{
	string result;
	for(int i=0;i<200;i++){
        result += "";
	}
	result += ("<<\n");
	result += (" mode: ");
	result += (mode);
	result += ("\n ecLevel: ");
	result += (ecLevel);
	result += ("\n version: ");
	result += (version);
	result += ("\n matrixWidth: ");
	result += (matrixWidth);
	result += ("\n maskPattern: ");
	result += (maskPattern);
	result += ("\n numTotalBytes: ");
	result += (numTotalBytes);
	result += ("\n numDataBytes: ");
	result += (numDataBytes);
	result += ("\n numECBytes: ");
	result += (numECBytes);
	result += ("\n numRSBlocks: ");
	result += (numRSBlocks);
	if (matrix == null)
	{
		result += ("\n matrix: null\n");
	}
	else
	{
		result += ("\n matrix:\n");
		result += (matrix->ToString());
	}
	result += (">>\n");
	return result;
}*/
bool QRCode::isValidMaskPattern(int maskPattern) {
    return maskPattern >= 0 && maskPattern < NUM_MASK_PATTERNS;
}
}
