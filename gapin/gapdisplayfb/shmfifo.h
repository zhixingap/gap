#ifndef __SHMFIFO_H__
#define __SHMFIFO_H__

#include <sys/shm.h>

#define ERR_EXIT(m)                             \
    do                                          \
    {                                           \
        perror(m);                              \
        exit(EXIT_FAILURE);                     \
    }while(0)

union semun{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
    struct seminfo *__buf;
};

typedef struct
{
    key_t key;
    unsigned int blksize; //sizeof(TYPE)
    unsigned int blocks;  //total of GAPImage
    unsigned int rd_idx;  //read index of share memory
    unsigned int wd_idx;  //write index of share memory
} shmhead_t;

typedef struct
{
    shmhead_t *p_shm; //the header of share memory
    char *p_payload; //first block
    int shmid;        //share memory id
    int sem_mutex;    //mutex for the share memory
    //the semaphore to control whether the share memory is full
    int sem_full;
    //the semaphore to control whether the share memory is empty
    int sem_empty;
} shmfifo_t;

int sem_create(key_t key);
int sem_open(key_t key);
int sem_setval(int semid, int val);
int sem_getval(int semid);
int sem_d(int semid);
int sem_p(int semid);
int sem_v(int semid);
void shmfifo_put(shmfifo_t *fifo, const void *buf);
void shmfifo_get(shmfifo_t *fifo, void *buf);
void shmfifo_destroy(shmfifo_t *fifo);

/* share memory struct for process.
|---------+--------+--------+--------+---+---+---+---+---+---+-----|
| blksize | blocks | rd_idx | wd_idx | 0 | 1 | 2 | 3 | 4 | 5 | ... |
|---------+--------+--------+--------+---+---+---+---+---+---+-----|
*/
shmfifo_t *shmfifo_init(const int key, const int blksize, int blocks);

#endif //__SHMFIFO_H__
