#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <string.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/time.h>

#include "mydepend/tinyxml.h"
#include "shmfifo.h"

#include <directfb/directfb.h>
#include <directfb/directfb_keynames.h>
#include "./cLog/log.h"
using namespace std;

typedef struct shm
{
    void *shm_head;
    key_t key;
    int shmid;
} shm_t;

vector<shmfifo_t *> vec_shm;

static char _flg_wirteloop_time[9]= {'0','0','0','0','0','0','0','0','\0'};
static char _flg_mainloop_time[9]= {'0','0','0','0','0','0','0','0','\0'};
static char _flg_getdata_time[9]= {'0','0','0','0','0','0','0','0','\0'} ;
static char _flg_decodesuc_time[9]= {'0','0','0','0','0','0','0','0','\0'} ;
static char _flg_sendsuc_time[9]= {'0','0','0','0','0','0','0','0','\0'} ;
static char _flg_sendSucCount[9]= {'0','0','0','0','0','0','0','0','\0'} ;

void releaseShareMemory(shmfifo_t *shm);

bool dataAvailableToBeRead(shmfifo_t *fifo);

void _flg_GetTime(char* _flg_time)
{
    time_t tmp=time(NULL);
    tm* tmpTime=localtime(&tmp);
    sprintf(_flg_time,"%02d:%02d:%02d",tmpTime->tm_hour,tmpTime->tm_min,tmpTime->tm_sec);

}
char* mem;

bool flag_CreateFile()
{
    int fd;
    string Path = "GAP_FLAG";
    if((fd=open(Path.c_str(),O_RDWR|O_CREAT, 0777))<0)
    {
        perror("open flagFile error");
        return false ;
    }
    lseek(fd,10000,SEEK_SET);
    write(fd,"a",1);

    mem=(char*)mmap(NULL,256,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    if(mem==MAP_FAILED)
    {
        perror("mmap flagFile error");
        return false;
    }
    return true;
}

//#define _RT ;
#define _FB ;
using namespace std;

#ifdef _FB
//这是最上层的接口，所有的函数入口均由它（IDirectFB）而来
static IDirectFB *dfb = NULL;

//主平面，也就是“屏幕”了。在交互层使用DFSCL_FULLSCREEN，它是主层平面。

static IDirectFBSurface *primary = NULL;

//static IDirectFBEventBuffer *keybuffer;

//这里存储主平面的高和宽，从而为其它的操作提供支持

static int screen_width = 0;

static int screen_height = 0;

#endif

#define GAP_DATA_LEN (600*600)

typedef struct
{
    int width;
    int height;
    unsigned char data[GAP_DATA_LEN + 1];
} GAPImage;

typedef struct
{
    int time ;// init:0 ; complete:1;
    int pos_x1;
    int pos_y1;
    int pos_x2;
    int pos_y2;
    int dst_width;
    string sharePath1_1;
    string sharePath1_2;
    string sharePath2_1;
    string sharePath2_2;
    string sharePath3_1;
    string sharePath3_2;
    string sharePath4;
    string sharePath5_1;  //higher (add heartbeat)
    string sharePath5_2;  //higher (add heartbeat)
} GAPDisplayConfig;

GAPDisplayConfig _config;
int _maxLen = 116;

void loadConfig();
void checkFile(string path);
shmfifo_t * getShareMemory(string keyPath, unsigned int blksize, unsigned int blocks);

void getImage(shmfifo_t *fifo_1, shmfifo_t *fifo_2, shmfifo_t *fifo_4,
               int &order, GAPImage *qrdata);

void getImage(shmfifo_t *fifo_1, shmfifo_t *fifo_2, shmfifo_t *fifo_4,
               int &order, GAPImage *qrdata)
{
    assert(qrdata != NULL); //stack var pass to this method, and return data
    assert(fifo_1 != NULL && fifo_2 != NULL);

    if (order == 1)
    {
        if (dataAvailableToBeRead(fifo_1))
        {
            shmfifo_get(fifo_1, qrdata);
            order = -1;
        }
        else if (dataAvailableToBeRead(fifo_2))
        {
            shmfifo_get(fifo_2, qrdata);
            order = 0;
        }
        else if (fifo_4 != NULL && dataAvailableToBeRead(fifo_4))
        {
            shmfifo_get(fifo_4, qrdata);
            order = 1;
        }
    }
    else if (order == -1)
    {
        if (dataAvailableToBeRead(fifo_2))
        {
            shmfifo_get(fifo_2, qrdata);
            order = 0;
        }
        else if (dataAvailableToBeRead(fifo_1))
        {
            shmfifo_get(fifo_1, qrdata);
            order = -1;
        }
        else if (fifo_4 != NULL && dataAvailableToBeRead(fifo_4))
        {
            shmfifo_get(fifo_4, qrdata);
            order = 1;
        }
    }
    else if (order == 0)
    {
        if (fifo_4 != NULL && dataAvailableToBeRead(fifo_4))
        {
            shmfifo_get(fifo_4, qrdata);
            order = 1;
        }
        else if (dataAvailableToBeRead(fifo_1))
        {
            shmfifo_get(fifo_1, qrdata);
            order = -1;
        }
        else if (dataAvailableToBeRead(fifo_2))
        {
            shmfifo_get(fifo_2, qrdata);
            order = 0;
        }
    }

    _flg_GetTime(_flg_getdata_time);
    memcpy(mem+10,_flg_getdata_time,8);
    memcpy(mem+18,"B ",2);
}

void drawMetrix(GAPImage *pos)
{
    if(NULL == pos)
    {
        cout << "pointer is null.." << endl;
    }

    int inputWidth = pos->width;
    int inputHeight = pos->height;


    //render
    int qrWidth = inputWidth + 8;
    int qrHeight = inputHeight + 8;
    // max(outWidth, qrWidth);
    int outputWidth = _config.dst_width>qrWidth?_config.dst_width:qrWidth;
    //max(outHeight, qrHeight);
    int outputHeight = _config.dst_width>qrHeight?_config.dst_width:qrHeight;

    //int multiple = min(outputWidth / qrWidth, outputHeight / qrHeight);//get rect size
    int tmpW = outputWidth / qrWidth;
    int tmpH=outputHeight / qrHeight;
    int multiple = tmpW<tmpH?tmpW :tmpH;

    //float offsetX = (screen_width-multiple*inputWidth)/2;
    float imageCenterX1 = multiple*inputWidth/2;
    float offsetX1 = _config.pos_x1-imageCenterX1;
    offsetX1 = offsetX1>0?offsetX1:0;
    float imageCenterY1 = multiple*inputHeight/2;
    float offsetY1 = _config.pos_y1-imageCenterY1;
    offsetY1 = offsetY1>0?offsetY1:0;

    float imageCenterX2 = multiple*inputWidth/2;
    float offsetX2 = _config.pos_x2-imageCenterX2;
    offsetX2= offsetX2>0?offsetX2:0;
    float imageCenterY2 = multiple*inputHeight/2;
    float offsetY2 = _config.pos_y2-imageCenterY2;
    offsetY2 = offsetY2>0?offsetY2:0;

    int y=0;
    int x=0;
    primary->SetColor (primary, 0x00, 0x00, 0x00, 0xff);

    for(y=0; y<inputHeight; y++)
    {
        for(x=0; x<inputWidth; x++)
        {
            if(pos->data[x+y*inputHeight]>0)
            {
//glRectf(offsetX+x*multiple,offsetY-y*multiple,offsetX+(x+1)*multiple,offsetY-(y-1)*multiple);
                primary->FillRectangle(primary,
                                       offsetX1+x*multiple,
                                       offsetY1+y*multiple,
                                       multiple,
                                       multiple);
                primary->FillRectangle(primary,
                                       offsetX2+x*multiple,
                                       offsetY2+y*multiple,
                                       multiple,
                                       multiple);


            }

        }
    }
}

void signal_handler(int s)
{
    char buffer[100];
    printf("Getted signal id=%d\n",s);
    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseShareMemory(vec_shm[i]);
    }
    vec_shm.clear();


    _exit(0);
}

int main (int argc, char **argv)
{
    signal(SIGINT,signal_handler);//Hook for ctrl+c and other key envents
    signal(SIGKILL,signal_handler);
    signal(SIGPIPE,SIG_IGN);

//size_t pagesize = sysconf(_SC_PAGESIZE);
#ifdef _RT
    RT_TASK* task;
    RT_TASK thread;
    int priority = 0;
    int stack_size = 0;
    int msg_size=0;
    RT_TASK *psMainBuddy;

    psMainBuddy = rt_task_init_schmod(nam2num("first"),0,stack_size,0,SCHED_FIFO,0);
    if(psMainBuddy ==NULL)
    {
        cout<<"error buddy"<<endl;
        exit(1);
    }

    //mlockall(MCL_CURRENT|MCL_FUTURE);
    rt_set_periodic_mode();
    int period = (int)nano2count((RTIME)10*1000*1000);
    start_rt_timer(period);
    rt_make_hard_real_time();
    rt_task_make_periodic(psMainBuddy,rt_get_time()+period,period);
#endif

    //日志等级　LL_DEBUG<LL_TRACE<LL_NOTICE<LL_WARNING<LL_ERROR
    bool LogInit(LogLevel l);
    LogInit(LL_DEBUG);

    string version = "3.5.0";
    if(argc>1)
    {
        if(strcmp(argv[1],"-v")==0)
        {
            cout<<version<<endl;
            return 0;
        }
        if(strcmp(argv[1],"--dfb:mode=640x480")!=0)
        {
            //return -100;
        }
    }

    struct sched_param param;
    int maxpri=sched_get_priority_max(SCHED_FIFO);

    if(maxpri!=-1)
    {
        param.__sched_priority=maxpri;
        if(sched_setscheduler(getpid(),SCHED_FIFO,&param)==-1)
        {
            printf("sched_setscheduler() failed");
            //LOG_ERROR("sched_setscheduler() failed    \n");
        }
    }
    else
    {
        printf("sched_get_priority_max() failed");
        //LOG_ERROR("sched_get_priority_max() failed    \n");
    }

    if(!flag_CreateFile()) return -4;
    loadConfig();

    checkFile(_config.sharePath1_1);
    checkFile(_config.sharePath1_2);
    checkFile(_config.sharePath2_1);
    checkFile(_config.sharePath2_2);
    checkFile(_config.sharePath3_1);
    checkFile(_config.sharePath3_2);
    checkFile(_config.sharePath4);
    checkFile(_config.sharePath5_1);
    checkFile(_config.sharePath5_2);

    shm_t shm;

    shmfifo_t *ori_p1_1 = getShareMemory(_config.sharePath1_1, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p1_1);
    if(ori_p1_1 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p1_2 = getShareMemory(_config.sharePath1_2, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p1_2);
    if(ori_p1_2 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p2_1 = getShareMemory(_config.sharePath2_1, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p2_1);
    if(ori_p2_1 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p2_2 = getShareMemory(_config.sharePath2_2, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p2_2);
    if(ori_p2_2 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p3_1 = getShareMemory(_config.sharePath3_1, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p3_1);
    if(ori_p3_1 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p3_2 = getShareMemory(_config.sharePath3_2, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p3_2);
    if(ori_p3_2 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p4 = getShareMemory(_config.sharePath4, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p4);
    if(ori_p4 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p5_1 = getShareMemory(_config.sharePath5_1, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p5_1);
    if(ori_p5_1 == NULL)
    {
        return -1;
    }

    shmfifo_t *ori_p5_2 = getShareMemory(_config.sharePath5_2, sizeof(GAPImage), _maxLen);
    vec_shm.push_back(ori_p5_2);
    if(ori_p5_2 == NULL)
    {
        return -1;
    }

    int clearFlag = 0;

#ifdef _FB
    //为了创建一个平面，需要定义一个平面描述子（surface description）

    DFBSurfaceDescription dsc;

   //初始化

    DirectFBInit (&argc, &argv);

    DirectFBCreate (&dfb);

    dfb->SetCooperativeLevel (dfb, DFSCL_FULLSCREEN);

    //设定dsc的一些属性，现在可以不用关心

    dsc.flags = DSDESC_CAPS;

    dsc.caps = (DFBSurfaceCapabilities)(DSCAPS_PRIMARY | DSCAPS_FLIPPING);

    //使用我们设定的dsc创建主平面（primary）

    dfb->CreateSurface( dfb, &dsc, &primary );

    //得到主平面的宽与高

    primary->GetSize (primary, &screen_width, &screen_height);

    //通过画一个和主平面同等大小的矩形来清空主平面；默认颜色为黑色

    //为了能显示画出来的线，先设置一下线的颜色，线的位置在屏幕的中间

    //primary->SetColor (primary, 0x80, 0x80, 0xff, 0xff);

    // primary->DrawLine (primary,0, 0,screen_width, screen_height);
    primary->SetColor (primary, 0xff, 0xff, 0xff, 0xff);
    primary->FillRectangle (primary,0 ,0 , screen_width ,screen_height);
    primary->Flip (primary, NULL, (DFBSurfaceFlipFlags)0);

//set clip area
    //DFBRegion clipRegion= {(screen_width-dstWidth)/2 ,(screen_height-dstHeight)/2,(screen_width+dstWidth)/2 ,(screen_height+dstHeight)/2};
    //primary->SetClip(primary,&clipRegion);

#endif

    GAPImage cur_qrdata;
    bool pri_p5_available, pri_p1_available, pri_p2_available, pri_p3_available;
    int order = 1;
    while(1)
    {
        _flg_GetTime(_flg_mainloop_time);
        memcpy(mem,_flg_mainloop_time,8);
        memcpy(mem+8,"A ",2);
        loadConfig();

        pri_p5_available = dataAvailableToBeRead(ori_p5_1) || dataAvailableToBeRead(ori_p5_2);
        pri_p1_available = dataAvailableToBeRead(ori_p1_1) || dataAvailableToBeRead(ori_p1_2) || dataAvailableToBeRead(ori_p4);
        pri_p2_available = dataAvailableToBeRead(ori_p2_1) || dataAvailableToBeRead(ori_p2_2);
        pri_p3_available = dataAvailableToBeRead(ori_p3_1) || dataAvailableToBeRead(ori_p3_2);

        if (pri_p5_available)
        {
            getImage(ori_p5_1, ori_p5_2, NULL, order, &cur_qrdata);
        }
        else if (pri_p1_available)
        {
            getImage(ori_p1_1, ori_p1_2, ori_p4, order, &cur_qrdata);

            clearFlag = 0;
        }
        else if (pri_p2_available)
        {
            getImage(ori_p2_1, ori_p2_2, NULL, order, &cur_qrdata);

            clearFlag = 0;
        }
        else if (pri_p3_available)
        {
            getImage(ori_p3_1, ori_p3_2, NULL, order, &cur_qrdata);

            clearFlag = 0;
        }
        else
        {
            _flg_GetTime(_flg_mainloop_time);
            memcpy(mem+20,_flg_mainloop_time,8);
            memcpy(mem+28,"C ",2);
            clearFlag++;
            if(clearFlag>3)
            {
                timeval delay1;
                delay1.tv_sec = 0;
                delay1.tv_usec = _config.time*2000;
                select(0,NULL,NULL,NULL,&delay1);
#ifdef _FB
                primary->SetColor (primary, 0xff, 0xff, 0xff, 0xff);
                    //primary->FillRectangle (primary,(screen_width-dstWidth)/2 ,(screen_height-dstHeight)/2 , dstWidth ,dstHeight);
                primary->FillRectangle(primary,0 ,0 , screen_width ,screen_height);
                primary->Flip (primary, NULL, (DFBSurfaceFlipFlags)0);
#endif
                clearFlag=0;
            }

            continue;
        }

#ifdef _FB
        primary->SetColor (primary, 0xff, 0xff, 0xff, 0xff);
        //primary->FillRectangle (primary,(screen_width-dstWidth)/2 ,(screen_height-dstHeight)/2 , dstWidth ,dstHeight);
        primary->FillRectangle(primary,0 ,0 , screen_width ,screen_height);

        drawMetrix(&cur_qrdata);
        _flg_GetTime(_flg_mainloop_time);
        memcpy(mem+30,_flg_mainloop_time,8);
        memcpy(mem+38,"D ",2);
#endif

#ifdef _FB

        _flg_GetTime(_flg_mainloop_time);
        memcpy(mem+40,_flg_mainloop_time,8);
        memcpy(mem+48,"E ",2);
        //显示
        primary->Flip (primary, NULL, (DFBSurfaceFlipFlags)0);
        _flg_GetTime(_flg_mainloop_time);
        memcpy(mem+50,_flg_mainloop_time,8);
        memcpy(mem+58,"F ",2);

#endif

#ifdef _RT
        rt_task_wait_period();
#else
        //usleep(_config.time*1000);
        timeval delay2;
        delay2.tv_sec = 0;
        delay2.tv_usec = _config.time*1000;
        select(0,NULL,NULL,NULL,&delay2);

        _flg_GetTime(_flg_mainloop_time);
        memcpy(mem+60,_flg_mainloop_time,8);
        memcpy(mem+68,"G ",2);
#endif

    }

#ifdef _FB
    primary->Release( primary );

    dfb->Release( dfb );

#endif

    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseShareMemory(vec_shm[i]);
    }

    return 0;
}

void checkFile(string path)
{
    if(access(path.c_str(),0)!=0)
    {
        int fd = open(path.c_str(),O_CREAT,O_RDWR);
        close(fd);
    }
}

shmfifo_t * getShareMemory(string keyPath, unsigned int blksize, unsigned int blocks)
{
    shmfifo_t *fifo = NULL;
    key_t key;

    key = ftok(keyPath.c_str(), 0);
    if (key == -1)
    {
        printf("%s\n","ftok error");
        LOG_ERROR("%s \n", "ftok error");
        exit(-1);
    }

    fifo = shmfifo_init(key, blksize, blocks);

    return fifo;
}

void releaseShareMemory(shmfifo_t *shm)
{
    shmfifo_destroy(shm);
}

void loadConfig()
{
    TiXmlDocument doc( "DisplayConfig.xml" );
    bool loadOkay = doc.LoadFile();
    if(loadOkay)
    {
        TiXmlElement* e0 = doc.RootElement();
        TiXmlElement* etime = e0->FirstChildElement("time");
        TiXmlAttribute* b1 = etime->FirstAttribute();
        string tmp = b1->Value();
        _config.time = atoi(tmp.c_str());

        etime = e0->FirstChildElement("x1");
        b1 = etime->FirstAttribute();
        tmp = b1->Value();
        _config.pos_x1 = atoi(tmp.c_str());

        etime = e0->FirstChildElement("y1");
        b1 = etime->FirstAttribute();
        tmp = b1->Value();
        _config.pos_y1 = atoi(tmp.c_str());

        etime = e0->FirstChildElement("x2");
        b1 = etime->FirstAttribute();
        tmp = b1->Value();
        _config.pos_x2 = atoi(tmp.c_str());

        etime = e0->FirstChildElement("y2");
        b1 = etime->FirstAttribute();
        tmp = b1->Value();
        _config.pos_y2 = atoi(tmp.c_str());

        etime = e0->FirstChildElement("width");
        b1 = etime->FirstAttribute();
        tmp = b1->Value();
        _config.dst_width = atoi(tmp.c_str());

        TiXmlElement* epath = e0->FirstChildElement("sharePath1_1");
        TiXmlAttribute* b2 = epath->FirstAttribute();
        _config.sharePath1_1 = b2->Value();

        epath = e0->FirstChildElement("sharePath1_2");
        b2 = epath->FirstAttribute();
        _config.sharePath1_2 = b2->Value();

        epath = e0->FirstChildElement("sharePath2_1");
        b2 = epath->FirstAttribute();
        _config.sharePath2_1 = b2->Value();

        epath = e0->FirstChildElement("sharePath2_2");
        b2 = epath->FirstAttribute();
        _config.sharePath2_2 = b2->Value();

        epath = e0->FirstChildElement("sharePath3_1");
        b2 = epath->FirstAttribute();
        _config.sharePath3_1 = b2->Value();

        epath = e0->FirstChildElement("sharePath3_2");
        b2 = epath->FirstAttribute();
        _config.sharePath3_2 = b2->Value();

        //add by lj 2017-6-19
        epath = e0->FirstChildElement("sharePath4");
        b2 = epath->FirstAttribute();
        _config.sharePath4 = b2->Value();

        //add by lj 2017-6-22
        epath = e0->FirstChildElement("sharePath5_1");
        b2 = epath->FirstAttribute();
        _config.sharePath5_1 = b2->Value();

        epath = e0->FirstChildElement("sharePath5_2");
        b2 = epath->FirstAttribute();
        _config.sharePath5_2 = b2->Value();
    }
}

bool LogInit(LogLevel l)
{
    //variable to construct the file name which including time stamp
    char makeFileName[64];

    time_t tmp = time(NULL);
    tm* tmpTime = localtime(&tmp);

    //detail filename, accurate to seconds
    snprintf(makeFileName, 64,"%d%02d%02d_%02d%02d%02d",
             tmpTime->tm_year+1900,
             tmpTime->tm_mon+1,
             tmpTime->tm_mday,
             tmpTime->tm_hour,
             tmpTime->tm_min,
             tmpTime->tm_sec);

    string Path = "logs_display/LOG_";
    Path.append(makeFileName);

    return log_init(l, Path.c_str(), "./");
}

bool dataAvailableToBeRead(shmfifo_t *fifo)
{
    int count = 0;

    sem_p(fifo->sem_mutex);
    count = sem_getval(fifo->sem_empty);
    sem_v(fifo->sem_mutex);

    return count > 0;
}
