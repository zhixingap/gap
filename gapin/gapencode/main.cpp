#include <iostream>
#include <libconfig.h++>
#include <vector>
#include <sstream>
#include "QRCodeWriter.h"

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <dirent.h>
#include <string.h>
#include <fstream>

#include <fcntl.h>
#include <sys/mman.h>
#include "tinyxml.h"
#include "shmfifo.h"

#include <pthread.h>

#include <deque>
#include <map>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> /*for struct sockaddr_in*/
#include <arpa/inet.h>
#include <stdio.h>
#include <math.h>
#include <signal.h>

#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>
#include "./cLog/log.h" //log function

using namespace std;
using namespace libconfig;

#define DEST_IP   "127.0.0.1"// localhost
//#define DEST_IP   "192.168.1.140"
//#define DEST_IP   "192.168.4.43"
#define DEST_PORT 8081
//#define _Debug

#define GAP_DATA_LEN (600*600)

typedef struct
{
    int width;
    int height;
    unsigned char data[GAP_DATA_LEN + 1];
} GAPImage;

typedef struct
{
    int width ;
    int height;
    int maxLen;
    int isFlip;
    int time;
    string outPutPath1_1;//high
    string outPutPath1_2;//high
    string outPutPath2_1;
    string outPutPath2_2;
    string outPutPath3_1;//low
    string outPutPath3_2;//low
    string outPutPath4;   //high (add File)
    string outPutPath5_1;   //higher (add heartbeat)
    string outPutPath5_2;   //higher (add heartbeat)
} EncodeConfig;

typedef struct shm
{
    void *shm_head;
    key_t key;
    int shmid;
} shm_t;

EncodeConfig _config;
zhixin::qrcode::QRCodeWriter _writer ;
zhixin::Ref<zhixin::ByteMatrix> _matrix; //存储二维码数据流

vector<shmfifo_t *> vec_shm;

int _maxLen = 116;
const int singlepart_len = 550; //二维码丰放的数据550字节，长内容分片用.

void checkFile(string path);
shmfifo_t * getShareMemory(string keyPath, unsigned int blksize, unsigned int blocks);
void releaseShareMemory(shmfifo_t *fifo);
void loadConfig();
bool QREncode(string content,int w,int h);
bool QREncode(char* content,int length,int w,int h);

bool getSourceStrSocket(char* content,int* ContentLenght);
void writeToDisplay(shmfifo_t *fifo);

//add by lj 2017/6/19
template<int N1,int N2, int N3>
int fileCopy(char (&buf)[N1], char (&src)[N2], char (des)[N3], float& filesize);

void pkg_Create_CompleteFile(char* buf_trans, int& buf_trans_len,
                             char* buf, struct dirent* ptr,
                             char* filename, float& filesize,
                             unsigned short int& fileID);

int verifyStr(char* data, int length);
int readFileList(char *basePath, shmfifo_t *fifo);

int split_File(shmfifo_t *fifo, char* src, float& filesize, int& split_num,
               char* split_num_total, char* split_num_part, int& split_num_id,
               char* filename, unsigned short int& fileID);

int DFADetect(string sensitiveWord,char *sourceContent); //敏感词检测

deque<string> _getCache;
int _cacheSize = 0;
void *getFileThread(void *param);
pthread_mutex_t _getMutex = PTHREAD_MUTEX_INITIALIZER;

int sockfd = -1;
pthread_mutex_t _sendMutex = PTHREAD_MUTEX_INITIALIZER;

void signal_handler(int s)
{
    char buffer[100];
    printf("Getted signal id=%d\n",s);
    for (int i = 0; i < vec_shm.size(); ++i)
    {
        releaseShareMemory(vec_shm[i]);
    }
    vec_shm.clear();

    close(sockfd);

    _exit(0);
}

//int sockfd = -1;
int initSocket(void)
{
    struct sockaddr_in dest_addr;
    int tmpSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (tmpSocket == -1)
    {
        perror("socket()");
        return -1;
    }
    /* 设置远程连接的信息*/
    dest_addr.sin_family = AF_INET;                 /* 注意主机字节顺序*/
    dest_addr.sin_port = htons(DEST_PORT);          /* 远程连接端口, 注意网络字节顺序*/
    dest_addr.sin_addr.s_addr = inet_addr(DEST_IP); /* 远程 IP 地址, inet_addr() 会返回网络字节顺序*/
    bzero(&(dest_addr.sin_zero), 8);                /* 其余结构须置 0*/

    int res = connect(tmpSocket, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in));
    if (res == -1)
    {
        perror("connect error");
        LOG_ERROR("Socket connect error    \n");
        close(tmpSocket);
        return -1;
    }
    return tmpSocket;
}
bool getSourceStrSocket(char* content,int* ContentLenght)
{
    pthread_mutex_lock(&_sendMutex);
    if(send(sockfd,"0\n",2,0)==-1)
    {
        pthread_mutex_unlock(&_sendMutex);
        perror("send error");
        LOG_ERROR("Socket send error    \n");
        close(sockfd);
        sockfd = initSocket();
        return false;
    }
    pthread_mutex_unlock(&_sendMutex);

    //delete by peck at [2018-03-19 15:16:57], too much log
    //LOG_NOTICE("main thread send 0 to java success...");

    //TODO:Java发过来的TCP数据会大于9000字节吗？还是每一次send都不会超过9000字节？
    char recvStr[9000] = {0};
    int res = recv(sockfd,recvStr,sizeof(recvStr),0);
    if (res == -1)
    {
        perror("recv error");
        LOG_ERROR("Socket recv error    \n");
        close(sockfd);
        sockfd = initSocket();
        return false;
    }

    //delete by peck at [2018-03-19 15:17:36], too much log
    //LOG_NOTICE("main thread receive data from java success...");

    //Java发过来的TCP数据报, 收到的字节流，前四个字节表示数据的长度(32位表示).
    int dataLength = ((recvStr[0]&0xff)<<24) | ((recvStr[1]&0xff)<<16) | ((recvStr[2]&0xff)<<8) | (recvStr[3]&0xff);

    //content=string(recvStr);
    //memcpy(content,recvStr,res);
    if(dataLength > 0)
    {
        //Tips:strTmp中间变量多余，可改成memcpy(content, recvStr + 4, dataLength);
        char *strTmp = new char[dataLength];
        memcpy(strTmp,(char*)&recvStr[4],dataLength);
        memset(&content[0], 0, 10500);
        memcpy(content,strTmp,dataLength);
        delete []strTmp;
        strTmp = NULL;
        *ContentLenght = dataLength;

        return true;
    }

    return false;
}

int RecvContentLenght;
int offset4 = 0;

int main(int argc,char* argv[])
{
    signal(SIGINT,signal_handler);//Hook for ctrl+c and other key envents
    signal(SIGKILL,signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGPIPE,SIG_IGN);
    loadConfig();

    //根据字节开始位判断字符是汉字还是字母数字等普通字符
    /*
    char aa[] = "一~-@+<>我 a置";
    int kk = strlen(aa);
    char tp = aa[3];
    if(tp&0x80)
    {
        cout << "chinese exit!" << endl;
    }
    */

    //
    //日志等级　LL_DEBUG<LL_TRACE<LL_NOTICE<LL_WARNING<LL_ERROR
    bool LogInit(LogLevel l);
    LogInit(LL_DEBUG);

    if(argc!=2)
    {
        printf("please input parameter (1 or 2 )    \n");
        LOG_WARN("please input parameter (1 or 2 )    \n");
        return -100;
    }
    string version = "3.5.0";
    if(strcmp(argv[1],"-v")==0)
    {
        cout<<version<<endl;
        return -100;
    }
    int camflg =atoi(argv[1]);

    shmfifo_t *shm_1, *shm_2, *shm_3, *shm_5;
    shm_1 = shm_2 = shm_3 = shm_5 = NULL;

    switch(camflg)
    {
    case 1:
        checkFile(_config.outPutPath1_1);
        checkFile(_config.outPutPath2_1);
        checkFile(_config.outPutPath3_1);
        checkFile(_config.outPutPath4);
        checkFile(_config.outPutPath5_1);

        shm_1 = getShareMemory(_config.outPutPath1_1,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_1);

        if(shm_1 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }
        shm_2 = getShareMemory(_config.outPutPath2_1,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_5);
        if(shm_2 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }
        shm_3 = getShareMemory(_config.outPutPath3_1,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_3);
        if(shm_3 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }

        //java端心跳共享内存区
        shm_5 = getShareMemory(_config.outPutPath5_1,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_5);
        if(shm_5 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }

        int temp;
        pthread_t _getFileT;
        pthread_attr_t attr;
        pthread_attr_init(&attr);

        //线程属性设置为detached，这样当线程结束时，系统会自动清理线程占用的资源，线程属性默认为joinable
        pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
        if((temp=pthread_create(&_getFileT,&attr,getFileThread,&camflg))!= 0)
        {
            printf("can't create thread: %s\n",strerror(temp));
            LOG_ERROR("can't create thread: %s\n",strerror(temp));
            return 1;
        }
        break;

    case 2:
        checkFile(_config.outPutPath1_2);
        checkFile(_config.outPutPath2_2);
        checkFile(_config.outPutPath3_2);
        checkFile(_config.outPutPath5_2);
        shm_1 = getShareMemory(_config.outPutPath1_2,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_1);

        if(shm_1 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }
        shm_2 = getShareMemory(_config.outPutPath2_2,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_5);
        if(shm_2 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }
        shm_3 = getShareMemory(_config.outPutPath3_2,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_5);
        if(shm_3 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }

        //java端心跳共享内存区
        shm_5 = getShareMemory(_config.outPutPath5_2,
                               sizeof(GAPImage), _maxLen);
        vec_shm.push_back(shm_5);
        if(shm_5 == NULL)
        {
            LOG_ERROR("Applying for shared memory occurs error   \n");
            return -1;
        }
        break;
    default:
        printf("please input parameter (1 or 2)    \n");
        LOG_WARN("please input parameter (1 or 2)    \n");
        return -100;
    }

    sockfd = initSocket(); //初始化socket,用于接收Java发过来的数据
    char content[10500] = {0}; //字符串最大支持10k字节发送

    struct tm* t;
    time_t tt;
    time(&tt);
    t = localtime(&tt);
    printf("main thread:localtime %4d%02d%02d %02d:%02d:%02d\n",
           t->tm_year+1900, t->tm_mon+1,t->tm_mday,t->tm_hour,t->tm_min,t->tm_sec);

    while(1)
    {
        time_t ts;
        time(&ts);
        if((ts-tt) >= 5)
        {
            tt = ts;
        }

        /* 获取Java发过来的数据报，并存在content中，数据长度为RecvContentLenght
         * 其中已经跳过前四个字节，前四个字节已在getSourceStrSocket返加到RecvContentLength
         * content的第一个字节为标志位：文件/传字符串的优先级高中低
         */
        if(getSourceStrSocket(content,&RecvContentLenght))
        {
            char priority = content[0];
            printf("java send data level:%c\n", priority);

            //长字符串分包
            float filesize = strlen(content)-1;
            //const int singlepart_len = 500;

            //二维码存放的数据550字节左右
            char buf_trans[singlepart_len+50] = {0};
            //长字符串切分(大字符串切分成小字符串,每个小字符串500字节)
            int split_num = ceil(filesize/singlepart_len);
            int split_num_id = 1;
            char split_num_total[2] = {0};
            char split_num_part[2] = {0};
            sprintf(split_num_total, "%02x", split_num);
            int pkg_total_length = 0;
            unsigned int split_chinese_byte_num = 0;
            unsigned int read_pos = 0;
            char unique_serialnumber[27] = {0};//Java发过来的数据中,27个字节的序列号
            int serialnumber_len = 27;

            for(int i=0; i<split_num; i++)
            {
                //printf("id: %d  before trans:%s\n", ++splitfileid, buf);
                sprintf(split_num_part, "%02x", split_num_id);

                //汉字分割处理
                unsigned int chinese_sign_num = 0;
                int read_pos_end = 0;

                if(0 == i)
                {
                    read_pos_end = read_pos + singlepart_len;
                }
                else
                {
                    read_pos_end = read_pos + singlepart_len -serialnumber_len;
                }

                for(int k=read_pos; k< read_pos_end; k++)
                {
                    if(content[k+1] & 0x80) //判断汉字个数从k+1起,跳过F标记.
                    {
                        chinese_sign_num++;
                    }
                }

                //分片读取完整一个汉字
                if((chinese_sign_num % 3) == 1)
                {
                    split_chinese_byte_num = 2;
                    //read_pos = read_pos + singlepart_len + split_chinese_byte_num;
                }
                else if((chinese_sign_num % 3) == 2)
                {
                    split_chinese_byte_num = 1;
                    //read_pos = read_pos + singlepart_len + split_chinese_byte_num;
                }
                else
                {
                    split_chinese_byte_num = 0;
                    //read_pos = read_pos + singlepart_len + split_chinese_byte_num;
                }

                int content_len = 0;
                if(i < split_num-1)
                {
                    //content_len = singlepart_len;
                    content_len = singlepart_len + split_chinese_byte_num;
                }
                else
                {
                    //content_len = filesize - i*singlepart_len;
                    content_len = filesize - read_pos;
                }

                char str[4];
                int pkg_head_len = 4;
                int sign_command_len = 1;
                int verifycode_len = 4;
                sprintf(str, "%04x", sign_command_len+sizeof(split_num_total)+sizeof(split_num_part)+content_len+verifycode_len);

                //分包的总长度, 第一个分包头已有序列号，第二个开始才要加上序列号的长度
                if(0 == i)
                {
                    pkg_total_length = pkg_head_len+sign_command_len+sizeof(split_num_total)+sizeof(split_num_part)+content_len+verifycode_len;
                }
                else
                {
                    pkg_total_length = pkg_head_len+sign_command_len+sizeof(split_num_total)+sizeof(split_num_part)+serialnumber_len+content_len+verifycode_len;
                }

                //用于计算出校验码
                memset(buf_trans, 0, sizeof(buf_trans));
                //memcpy(buf_trans, content+1+i*singlepart_len, content_len); //从优先级字符后面开始分包
                memcpy(buf_trans, content+1+read_pos, content_len); //从优先级字符后面开始分包

                int verifycode = verifyStr(buf_trans, strlen(buf_trans));
                char strc[4];
                sprintf(strc, "%04x", verifycode);  //4个字节的校验码，16进制表示
                memset(buf_trans, 0, sizeof(buf_trans));
                memcpy(buf_trans, str, sizeof(str)); //加上字符串长度
                memcpy(buf_trans+sizeof(str), &priority, 1);  //加上标示符,文件为F,字符串为优先级
                memcpy(buf_trans+sizeof(str)+1, split_num_total, 2);  //加上字符串切分总数total
                memcpy(buf_trans+sizeof(str)+1+2, split_num_part, sizeof(split_num_part));  //加上字符串切分子字符串编号part
                //memcpy(buf_trans+sizeof(str)+1+2+2, content+1+i*singlepart_len, content_len); //加上字符串内容
                if(0 == i)
                {
                    memcpy(unique_serialnumber, content+1+read_pos, serialnumber_len);
                    memcpy(buf_trans+sizeof(str)+1+2+2, content+1+read_pos, content_len); //加上字符串内容
                }
                else
                {
                    memcpy(buf_trans+sizeof(str)+1+2+2, unique_serialnumber, serialnumber_len); //加上字符串唯一序列号，各分包之间序列号相同
                    memcpy(buf_trans+sizeof(str)+1+2+2+serialnumber_len, content+1+read_pos, content_len); //加上字符串内容
                }

                if(0 == i)
                {
                    memcpy(buf_trans+sizeof(str)+1+2+2+content_len, strc, sizeof(strc));  //加上校验码
                }
                else
                {
/*
分包发送数据格式
|------------+------+-------------+----------------+--------------+--------------------+----------|
|          4 |    1 |           2 |              2 |           27 | content bytes      | 4 bytes  |
|------------+------+-------------+----------------+--------------+--------------------+----------|
| pkg length | flag | total slice | slice sequence | unique sn id | content to be send | checksum |
|------------+------+-------------+----------------+--------------+--------------------+----------|
*/
                    memcpy(buf_trans+sizeof(str)+1+2+2+serialnumber_len+content_len, strc, sizeof(strc));  //加上校验码
                }

                if(buf_trans[0] == 0)
                {
                    printf("string after trans:%s\n", &buf_trans[1]);
                    //LOG_DEBUG("string after trans:%s\n", &buf_trans[1]);
                }
                else
                {
                    printf("string after trans:%s\n", buf_trans);
                    //LOG_DEBUG("string after trans:%s\n", buf_trans);
                }
                if(split_num_id < split_num)
                {
                    split_num_id++; //字符串切分编号自增１,接下来读字符串的下一个子字符串(500字节一个子字符串)
                }

                bool res = QREncode(buf_trans, pkg_total_length, _config.width, _config.height);

                if(res)//Write To Display
                {
                    if(priority == '4') //higher, heart beat
                    {
                        writeToDisplay(shm_5);
                    }
                    else if(priority == '3')//high
                    {
                        writeToDisplay(shm_1);
                    }
                    else if(priority == '2')
                    {
                        writeToDisplay(shm_2);
                    }
                    else if(priority == '1')//low
                    {
                        writeToDisplay(shm_3);
                    }
                    else
                    {
                        printf("***************get priority error****************\n");
                        LOG_DEBUG("***************get priority error****************\n");
                    }
                }
                else  //Write To Display
                {
                    printf("***************encode error****************\n");
                    //LOG_DEBUG("***************encode error****************\n");
                }////Write To Display  END

                read_pos = read_pos + singlepart_len + split_chinese_byte_num;
                usleep(10000);
            }
        }
        else
        {
            //cout << "no data" << endl;
            usleep(100*1000);
        }
    }

    releaseShareMemory(shm_1);
    releaseShareMemory(shm_2);
    releaseShareMemory(shm_3);
    releaseShareMemory(shm_5);

    close(sockfd);
    INFO_W.logclose();
    WARN_W.logclose();

    return 0;
}

void writeToDisplay(shmfifo_t *fifo)
{
    GAPImage gap;

    gap.width = _matrix->getWidth();
    gap.height = _matrix->getHeight();
    _matrix->ToUnsignedCharPixel(gap.data);

    shmfifo_put(fifo, &gap);
}

void checkFile(string path)
{
    if(access(path.c_str(),0)!=0)
    {
        int fd = open(path.c_str(),O_CREAT,O_RDWR);
        close(fd);
    }
}

shmfifo_t * getShareMemory(string keyPath, unsigned int blksize, unsigned int blocks)
{
    shmfifo_t *fifo = NULL;

    key_t key = ftok(keyPath.c_str(), 0);
    if (key == -1)
    {
        printf("%s\n","ftok error");
        LOG_ERROR("%s \n", "ftok error");
        exit(-1);
    }

    fifo = shmfifo_init(key, blksize, blocks);

    return fifo;
}

void releaseShareMemory(shmfifo_t *fifo)
{
    shmfifo_destroy(fifo);
}

void loadConfig()
{
    TiXmlDocument doc( "EncodeConfig.xml" );
    bool loadOkay = doc.LoadFile();
    if(loadOkay)
    {
        TiXmlElement* e0 = doc.RootElement();
        TiXmlElement* e1 = e0->FirstChildElement("width");
        TiXmlAttribute* a1 = e1->FirstAttribute();
        string tmp = a1->Value();
        _config.width = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("height");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.height = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("maxLen");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.maxLen = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("isFlip");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.isFlip = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("time");
        a1 = e1->FirstAttribute();
        tmp = a1->Value();
        _config.time = atoi(tmp.c_str());

        e1 = e0->FirstChildElement("outPutPath1_1");
        a1 = e1->FirstAttribute();
        _config.outPutPath1_1 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath1_2");
        a1 = e1->FirstAttribute();
        _config.outPutPath1_2 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath2_1");
        a1 = e1->FirstAttribute();
        _config.outPutPath2_1 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath2_2");
        a1 = e1->FirstAttribute();
        _config.outPutPath2_2 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath3_1");
        a1 = e1->FirstAttribute();
        _config.outPutPath3_1 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath3_2");
        a1 = e1->FirstAttribute();
        _config.outPutPath3_2 = a1->Value();

        //add by lj 2017/6/19 oriented to file
        e1 = e0->FirstChildElement("outPutPath4");
        a1 = e1->FirstAttribute();
        _config.outPutPath4 = a1->Value();

        //add by lj 2017/6/22 oriented to heartbeat
        e1 = e0->FirstChildElement("outPutPath5_1");
        a1 = e1->FirstAttribute();
        _config.outPutPath5_1 = a1->Value();

        e1 = e0->FirstChildElement("outPutPath5_2");
        a1 = e1->FirstAttribute();
        _config.outPutPath5_2 = a1->Value();

    }
}

bool QREncode(string content,int w,int h)
{
    try
    {
        _matrix = _writer.encode(content,w,h,"");
    }
    catch(zhixin::Exception e)
    {
        printf("QREncode exception \n");
        LOG_ERROR("QREncode exception \n");
        return false;
    }
    return true;
}

bool QREncode(char* content,int length,int w,int h)
{
    try
    {
        _matrix = _writer.encode(content,length,w,h,"");
        int wd = _matrix->getWidth();
        int ht = _matrix->getHeight();
        printf("width:%d\n", wd);
        printf("height:%d\n", ht);
        //int a = 1+2;
    }
    catch(zhixin::Exception e)
    {
        printf("QREncode exception \n");
        LOG_ERROR("QREncode exception \n");
        return false;
    }
    return true;
}

void *getFileThread(void *param)
{
    shmfifo_t *fifo = NULL;

    fifo = getShareMemory(_config.outPutPath4,
                          sizeof(GAPImage), _maxLen);
    vec_shm.push_back(fifo);

    char basePath[1000];

    ///get the current absoulte path
    memset(basePath,'\0',sizeof(basePath));
    getcwd(basePath, 999);
    char relativePath[20] = "/sftp-remote";
    memcpy(basePath+strlen(basePath), relativePath, strlen(relativePath));
    printf("the current dir is : %s\n",basePath);
    LOG_DEBUG("the current dir is : %s\n",basePath);

    readFileList(basePath, fifo);

    releaseShareMemory(fifo);

    return NULL;
}

int verifyStr(char* data, int length)
{
    int crc16 = 0xffff;

    for (int i = 0;i < length;i++)
    {
        char tempx = data[i]&0xFF;
        crc16 = crc16 ^ tempx;
        for (int j = 0;j < 8;j++)
        {
            if (crc16 & 0x01)
                crc16 = (crc16 >> 1) ^ 0xa001;
            else crc16 = crc16 >> 1;
        }
    }
    return crc16;
}

int readFileList(char *basePath, shmfifo_t *fifo)
{
    unsigned short int fileID = 1;
    int sensitiveflag = -1;

    struct tm* t;
    time_t tt;
    time_t ts;
    time(&tt);
    t = localtime(&tt);
    printf("child thread:localtime %4d%02d%02d %02d:%02d:%02d\n",
           t->tm_year+1900, t->tm_mon+1,t->tm_mday,t->tm_hour,t->tm_min,t->tm_sec);

    while(1)
    {
        time_t ts;
        time(&ts);
        if((ts-tt) >= 5)
        {
            tt = ts;
        }

        DIR *dir;
        struct dirent *ptr;
        char src_path[256] = "sftp-remote/";
        char des_path[256] = "sftp-remote-bk/";

        if ((dir=opendir(basePath)) == NULL)
        {
            perror("Open file dir error...");
            LOG_ERROR("Open file dir error...\n");
            exit(1);
        }

        //LOG_NOTICE("while 1 in once more");

        while ((ptr=readdir(dir)) != NULL)
        {
            if(ptr->d_name[0] == '.')
            {
                continue;
            }
            memset(src_path, 0 ,sizeof(src_path));
            memset(des_path, 0, sizeof(src_path));
            memcpy(src_path, "sftp-remote/", 12);
            memcpy(des_path, "sftp-remote-bk/", 15);
            printf("filename:%s/%s\n",basePath,ptr->d_name);
            LOG_NOTICE("filename:%s/%s\n",basePath,ptr->d_name);
            memcpy(src_path+12,ptr->d_name,strlen(ptr->d_name));
            printf("debug src_path: %s    %s\n", src_path, ptr->d_name);
            LOG_NOTICE("debug src_path: %s    %s\n", src_path, ptr->d_name);
            memcpy(des_path+15,ptr->d_name,strlen(ptr->d_name));

            if (strstr(des_path, ".") == NULL) //no suffix
            {
                strcat(des_path, ".txt");
            }

            //文件拷贝 每个文件最多100k
            char buff[102400] = {0};
            float filesize = 0;
            sensitiveflag = fileCopy<102400, 256, 256>(buff, src_path, des_path, filesize);
            if(sensitiveflag >= 0)
            {
                cout << "Can't be sent,because sensitive word exits int file content..." << endl;
                LOG_WARN("Can't be sent,because sensitive word exits int file content...\n");
                break;
            }

            //对整个文本文件内容的属性编包
            //int verifycode = 0;
            char buff_trans[105000] = {0};
            int buff_trans_len = 0;
            char file_name[256] = {0}; //max filename's len in windows
            pkg_Create_CompleteFile(buff_trans, buff_trans_len, buff, ptr, file_name, filesize, fileID);
            memset(buff, 0, sizeof(buff));

            //文件切分(大文件切分成小文件,每个小文件500字节)
            int split_num = ceil(filesize/singlepart_len);
            int split_num_id = 1;
            char split_num_total[2] = {0};
            char split_num_part[2] = {0};
            sprintf(split_num_total, "%02x", split_num);
            int rst = split_File(fifo, src_path, filesize, split_num,
                                 split_num_total, split_num_part,
                                 split_num_id, file_name, fileID);

            if(rst == -1)
            {
                break;
            }

            //回传文件给java端
            pthread_mutex_lock(&_sendMutex);
            if(send(sockfd,buff_trans,buff_trans_len,0)==-1)
            {
                pthread_mutex_unlock(&_sendMutex);
                perror("send error");
                LOG_ERROR("back send error");
                close(sockfd);
                sockfd = initSocket();
                break;
            }
            if(send(sockfd,"\n",1,0)==-1)
            {
                pthread_mutex_unlock(&_sendMutex);
                perror("send error");
                LOG_ERROR("back send error");
                close(sockfd);
                sockfd = initSocket();
                break;
            }
            pthread_mutex_unlock(&_sendMutex);

            //cout << "fileid: " << fileID << endl;
            if(fileID < 65535) //每传输完5000个文件，就建立一个新的日志文件，避免单个日志文件过大
            {
                fileID++; //文件id自增,切换读下一个文件
                //LOG_DEBUG("fileID: %d\n",fileID);
                if( 0 == (fileID % 5000))
                {
                    //LOG_DEBUG("fileID: %d\n",fileID);
                    //日志等级　LL_DEBUG<LL_TRACE<LL_NOTICE<LL_WARNING<LL_ERROR
                    bool LogInit(LogLevel l);
                    LogInit(LL_DEBUG);
                }
            }
            else
            {
                fileID = 1;
            }


            //文件删除
            if((access(src_path,F_OK))!=-1)
            {
                if( remove(src_path) == 0 )
                {
                    printf("Removed %s.\n", src_path);
                    LOG_DEBUG("Removed %s.\n", src_path);
                }
                else
                {
                    perror("remove");
                    LOG_ERROR("remove error\n");
                }
            }
        }

        if(sensitiveflag >= 0)
        {
            //文件删除
            if((access(src_path,F_OK))!=-1)
            {
                if( remove(src_path) == 0 )
                {
                    printf("Removed %s.\n", src_path);
                    LOG_DEBUG("Removed %s.\n", src_path);
                }
                else
                {
                    perror("remove");
                    LOG_ERROR("remove error\n");
                }
            }
        }

        sensitiveflag = -1;
        closedir(dir);
    }
    return 1;
}

template<int N1,int N2, int N3>
int fileCopy(char (&buf)[N1], char (&src)[N2], char (des)[N3], float& filesize)
{
    //char abc[10240];
    //int aa = sizeof(abc);
    int tempsize = 0;
    int position = -1;
    FILE *in,*out;
    in = fopen(src,"r+");
    if(in == NULL)
    {
        perror(src);
        LOG_ERROR("src file open failed....%s\n", strerror(errno));
    }
    out = fopen(des,"w+");
    if(out == NULL)
    {
        perror(des);
        LOG_ERROR("des file open failed....%s\n", strerror(errno));
    }

    while((tempsize = fread(buf, 1, N1, in)))
    {
        Config cfg;

        /*
          解析配置文件
        */
        try
        {
          cfg.readFile("sensitive.conf");
        }
        catch(const FileIOException &fioex)
        {
          std::cerr << "I/O error while reading file." << std::endl;
          return -2;
        }
        catch(const ParseException &pex)
        {
          std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
                    << " - " << pex.getError() << std::endl;
          return -2;
        }

        /* 从配置文件中得到version这个配置项的值 */
        try
        {
            string sensitive_word = cfg.lookup("sensitive");
            //int abc = 1;
            cout << "sensitive word: " << sensitive_word << endl << endl;
            vector<string> ss;
            stringstream sstr(sensitive_word);
            string token;

            while(getline(sstr, token, ','))
            {
               ss.push_back(token);
               //int x = 0;
            }

            for(int i=0; i<ss.size(); i++)
            {
                position=DFADetect(ss.at(i),buf);
                if(position >= 0)
                {
                    cout<<"find sensitive word "<<ss.at(i)<<" position:"<<position<<endl;
                    break;
                }
                else
                {
                    cout<<"finished:"<<ss.at(i)<<endl;
                }
            }
            //int endzz = 0;
        }
        catch(const SettingNotFoundException &nfex)
        {
            cerr << "No 'version' setting in configuration file." << endl;
        }

        filesize = tempsize;
        fwrite(buf,1,filesize,out);
    }
    fflush(in);
    fflush(out);
    fclose(in);
    fclose(out);

    return position;
}



/*
传文件,发送文件的属性(回传给Java,告诉其发了什么东西)
|------------+--------+--------------+---------+----------|
|          4 |      1 | filename_len |       4 |        4 |
|------------+--------+--------------+---------+----------|
| pkg length | flag:F | filename     | file id | checksum |
|------------+--------+--------------+---------+----------|
*/
void pkg_Create_CompleteFile(char* buf_trans, int& buf_trans_len, char* buf, struct dirent* ptr, char* filename, float& filesize, unsigned short int& fileID)
{
    int command_len = 1;
    int verifycode = verifyStr(buf, strlen(buf));
    char vrfycode[4];
    sprintf(vrfycode, "%04x", verifycode);
    memcpy(filename, ptr->d_name, strlen(ptr->d_name));
    char filepath[300] = "sftp-remote-bk/"; //文件路径
    memcpy(&filepath[15], filename, strlen(filename));
    //int completefile_len = filesize + command_len + strlen(filename) + sizeof(verifycode);
    int completefile_len = command_len + strlen(filename) + 4 + sizeof(verifycode); //其中４个字节为文件id编号
    char comp_head[4]; //回传给Java, 告诉其发文件的属性，头4个字节表示属性信息的长度.
    sprintf(comp_head, "%04x", completefile_len);
    //memcpy(buf_trans, buf, filesize);
    //memset(buff, 0 ,sizeof(buff));
    memcpy(buf_trans, comp_head, sizeof(comp_head)); //加上长度
    memcpy(buf_trans+sizeof(comp_head), "F", command_len);  //加上命令符F,表示文件
    memcpy(buf_trans+sizeof(comp_head)+command_len, filename, strlen(filename));  //加上文件名
    //memcpy(buf_trans+sizeof(comp_head)+command_len+strlen(filename), buf, (int)filesize);  //加上文件内容
    //memcpy(buf_trans+sizeof(comp_head)+command_len+strlen(filename), filepath, strlen(filepath));  //加上文件路径
    char file_id[4] = {0};
    sprintf(file_id,"%04d",fileID);
    memcpy(buf_trans+sizeof(comp_head)+command_len+strlen(filename), file_id, sizeof(file_id)); //加上文件id
    //memcpy(buf_trans+sizeof(comp_head)+command_len+strlen(filename)+(int)filesize, vrfycode, sizeof(vrfycode));  //加上校验码
    memcpy(buf_trans+sizeof(comp_head)+command_len+strlen(filename)+sizeof(file_id), vrfycode, sizeof(vrfycode));  //加上校验码
    //buf_trans_len = sizeof(comp_head)+command_len+strlen(filename)+filesize+sizeof(vrfycode);
    buf_trans_len = sizeof(comp_head)+command_len+strlen(filename)+sizeof(file_id)+sizeof(vrfycode);
}

int split_File(shmfifo_t *fifo, char* src, float& filesize, int& split_num,
               char* split_num_total, char* split_num_part, int& split_num_id,
               char* filename, unsigned short int& fileID)
{
    FILE *fp = fopen(src, "r");
    if (fp == NULL)
    {
        LOG_ERROR("open file:%s error, error is %s\n!", src, strerror(errno));
        return -1;
    }

    char buf[singlepart_len+10]; //多余字节用来存储被切分的汉字剩余的字节，经测试：一个汉字在此linux系统中占用３个字节
    memset(buf, 0, sizeof(buf));

    //read返回０表示文件读取完毕
    //while循环每次从一个文件读500字节,在此while循环里面,文件id相同,当退出此while循环之后,文件id自增１
    int splitfileid = 0;
    while(fread(buf, sizeof(char), singlepart_len, fp) > 0) {
        sprintf(split_num_part, "%02x", split_num_id);

        //汉字分割处理
        unsigned int chinese_sign_num = 0;
        for(int k = 0; k < singlepart_len; k++)
        {
            if(buf[k] & 0x80)
            {
                chinese_sign_num++;
            }
        }

        //extra bytes of the last chinese
        if((chinese_sign_num % 3) == 1)
        {
//            read(fd, &buf[singlepart_len], 2);
            fread(buf + singlepart_len, sizeof(char), 2, fp);
        }
        if((chinese_sign_num % 3) == 2)
        {
//            read(fd, &buf[singlepart_len], 1);
            fread(buf + singlepart_len, sizeof(char), 1, fp);
        }

        int lenstr = strlen(buf);

        char *p = new char[1];
        p[0] = 0;
        p[0] = buf[lenstr-1];
        if(strcmp("\n",p) == 0)
        {
            buf[lenstr-1] = 0;
        }
        delete [] p;
        p = NULL;

        lenstr = strlen(buf);
        printf("id: %d  before trans:%s\n", ++splitfileid, buf);
        //LOG_NOTICE("id: %d  before trans:%s\n", splitfileid, buf);
        //unsigned char tp_str[2];
        char file_id[4] = {0};
        char str[4];
        int pkg_head_len = 4;
        int sign_command_len = 1;
        int verifycode_len = 4;

        static int _split_num_total = 2;
        static int _split_num_part = 2;
        static int _str_len = 0;

        _str_len = sign_command_len + sizeof(file_id) + _split_num_total + _split_num_part + strlen(filename) + lenstr + verifycode_len;
        sprintf(str, "%04x", _str_len);

        int pkg_total_length = pkg_head_len+sign_command_len + sizeof(file_id) + _split_num_total + _split_num_part + strlen(filename) + lenstr + verifycode_len;

        char buf_trans[1000];
        memset(buf_trans, 0, sizeof(buf_trans));
        memcpy(buf_trans, str, sizeof(str)); //加上字符串长度
        int verifycode = verifyStr(buf, strlen(buf));
        char strc[4];
        sprintf(strc, "%04x", verifycode);
        memcpy(buf_trans+sizeof(str), "F", 1);  //加上标示符
        //cout << "--------------trans before------------------ FILE ID: " << fileID << endl;
        sprintf(file_id,"%04x",fileID);
        memcpy(buf_trans+sizeof(str)+1, file_id, sizeof(file_id));      //加上文件id
        memcpy(buf_trans+sizeof(str)+1+sizeof(file_id), split_num_total, 2);  //加上文件切分总数total
        memcpy(buf_trans+sizeof(str)+1+sizeof(file_id)+2, split_num_part, 2);  //加上文件切分子文件编号part
        memcpy(buf_trans+sizeof(str)+1+sizeof(file_id)+2+2, filename, strlen(filename)); //加上文件名
        memcpy(buf_trans+sizeof(str)+1+sizeof(file_id)+2+2+strlen(filename), buf, lenstr); //加上文件内容
        memcpy(buf_trans+sizeof(str)+1+sizeof(file_id)+2+2+strlen(filename)+lenstr, strc, sizeof(strc));  //加上校验码
        if(buf_trans[0] == 0)
        {
            printf("id: %d  after trans:%s\n", splitfileid, &buf_trans[1]);
            LOG_WARN("id: %d  after trans:%s\n", splitfileid, &buf_trans[1]);
            LOG_NOTICE("fileID: %d   fileNAME: %s   Total: %s   Part: %s\n", fileID, filename, split_num_total, split_num_part);
        }
        else
        {
            printf("id: %d  after trans:%s\n", splitfileid, buf_trans);
            LOG_WARN("id: %d  after trans:%s\n", splitfileid, buf_trans);
            LOG_NOTICE("fileID: %d   fileNAME: %s   Total: %s   Part: %s\n", fileID, filename, split_num_total, split_num_part);
        }

        //将读取到的文件内容发送出去 add by lj 2017/6/19
        bool res = QREncode((char*)&buf_trans[0],pkg_total_length,_config.width,_config.height);

        
        if(res)//Write To Display:write file data.
        {
            writeToDisplay(fifo);
        }
        else  //Write To Display
        {
            printf("***************encode error****************\n");
            LOG_ERROR("***************encode error****************\n");
        }////Write To Display  END

        if(split_num_id < split_num)
        {
            split_num_id++; //文件切分编号自增１,接下来读文件的下一个子文件(550字节一个子文件)
        }
        memset(buf, 0, sizeof(buf));
        memset(buf_trans, 0, sizeof(buf_trans));
    }

    fclose(fp);
    return 0;
}

int DFADetect(string sensitiveWord,char *sourceContent)
{
    int found = -1;
    const char *sensitive_str = sensitiveWord.c_str();

    if (strlen(sensitive_str) > 0)
    {
        found = strcasestr(sourceContent, sensitive_str) == NULL ? -1 : 1;
    }

    return found;
}

bool LogInit(LogLevel l)
{
    //variable to construct the file name which including time stamp
    char makeFileName[64];

    time_t tmp = time(NULL);
    tm* tmpTime = localtime(&tmp);

    //detail filename, accurate to seconds
    snprintf(makeFileName, 64,"%d%02d%02d_%02d%02d%02d",
             tmpTime->tm_year+1900,
             tmpTime->tm_mon+1,
             tmpTime->tm_mday,
             tmpTime->tm_hour,
             tmpTime->tm_min,
             tmpTime->tm_sec);

    string Path = "logs_encode/LOG_";
    Path.append(makeFileName);

    return log_init(l, Path.c_str(), "./");
}
