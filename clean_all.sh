#!/bin/bash

top_dir=`pwd`

cd $top_dir
dir=`find . -name 'TestEncode.cbp' -print | sed 's:/TestEncode.cbp::'`
cd $dir && make clean

cd $top_dir
dir=`find . -name 'GAPEncode.cbp' -print | sed 's:/GAPEncode.cbp::'`
cd $dir && make clean

cd $top_dir
dir=`find . -name 'GAPDisplayFB.cbp' -print | sed 's:/GAPDisplayFB.cbp::'`
cd $dir && make clean

cd $top_dir
dir=`find . -name 'TestDecode.cbp' -print | sed 's:/TestDecode.cbp::'`
cd $dir && make clean

cd $top_dir
dir=`find . -name 'GAPCamera.cbp' -print | sed 's:/GAPCamera.cbp::'`
cd $dir && make clean

cd $top_dir
dir=`find . -name 'GAPDecode.cbp' -print | sed 's:/GAPDecode.cbp::'`
cd $dir && make clean
