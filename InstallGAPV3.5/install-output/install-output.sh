#!/bin/bash
basepath=$(cd `dirname $0`; pwd)
echo $basepath
####### FIREWALL ###############
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=8080/tcp --permanent
firewall-cmd --reload
######### NET-TOOLS ##############
cd $basepath/pkg-net-tools
rpm -ivh --force *.rpm
########## NETWORK #############
sed -i 's/BOOTPROTO=dhcp/BOOTPROTO=static/g' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i 's/IPV6INIT=yes/IPV6INIT=no/g' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i 's/ONBOOT=no/ONBOOT=yes/g' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i '$a IPADDR=192.168.3.10' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i '$a GATEWAY=192.168.3.1' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sed -i '$a NETMASK=255.255.255.0' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
mac1=$(cat /sys/class/net/enp0s31f6/address)
echo $mac1
sed -i '$a HWADDR='"$mac1"'' /etc/sysconfig/network-scripts/ifcfg-enp0s31f6
sleep 2
##
sed -i 's/BOOTPROTO=dhcp/BOOTPROTO=static/g' /etc/sysconfig/network-scripts/ifcfg-enp4s0
sed -i 's/IPV6INIT=yes/IPV6INIT=no/g' /etc/sysconfig/network-scripts/ifcfg-enp4s0
sed -i 's/ONBOOT=no/ONBOOT=yes/g' /etc/sysconfig/network-scripts/ifcfg-enp4s0
sed -i '$a IPADDR=192.168.3.11' /etc/sysconfig/network-scripts/ifcfg-enp4s0
sed -i '$a GATEWAY=192.168.3.1' /etc/sysconfig/network-scripts/ifcfg-enp4s0
sed -i '$a NETMASK=255.255.255.0' /etc/sysconfig/network-scripts/ifcfg-enp4s0
mac2=$(cat /sys/class/net/enp4s0/address)
echo $mac2
sed -i '$a HWADDR='"$mac2"'' /etc/sysconfig/network-scripts/ifcfg-enp4s0
sleep 2
systemctl stop NetworkManager
systemctl disable NetworkManager
systemctl restart network
########### JDK ############
cd $basepath/pkg-jdk
rpm -ivh --force *.rpm
######## MARIADB #########
cd $basepath/pkg-mariadb
# only used mariadb10
#yum remove -y mariadb-libs
rpm -ivh --force *.rpm
###
# only used mariadb10
#systemctl start mysql
# only used mariadb5
systemctl enable mariadb
systemctl start mariadb
mysql <<EOF
DROP DATABASE IF EXISTS gapwebmin;
CREATE DATABASE gapwebmin DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
EOF
######### USB #########
cd $basepath/pkg-usbutils
rpm -ivh --force *.rpm
######### OPENCV ##########
cd $basepath/pkg-opencv
rpm -ivh --force *.rpm
########## GAP #############
mkdir /home/update_dir
cd $basepath
\cp -rf GAP_OUT /home
cd /home/GAP_OUT
tar -xzvf GAPJetty.tar.gz
cd GAPJetty/webapps
mkdir GAPWebmin
cd GAPWebmin
jar -xvf /home/GAP_OUT/GAPWebmin_output.war
cd /home/GAP_OUT
rm -f GAPJetty.tar.gz
rm -f GAPWebmin.war
cd /home
chmod -R 777 GAP_OUT
################ start on boot ##############################
sed -i '$a sh /home/GAP_OUT/start.sh' /etc/rc.d/rc.local
chmod 777 /etc/rc.d/rc.local

echo "###############################################################"
echo "###################### INSTALL END ############################"
echo "###############################################################"
