#!/bin/bash

path="sftp-remote"
while true
do
  for file in sftp_file/*
  do
    if [ -f "$file" ] && [ -r "$file" ]
    then
      echo $file
      mv "$file" $path
    fi
  done
  sleep 2
done
