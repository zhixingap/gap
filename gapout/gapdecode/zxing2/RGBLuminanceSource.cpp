#include "RGBLuminanceSource.h"
#include "common/IllegalArgumentException.h"
namespace zxing2
{
/*
RGBLuminanceSource::RGBLuminanceSource(unsigned char* d, int W, int H)
{
    isRotated = false;
_isQRcodeImage = true;
    __width = W;
    __height = H;
    int width = W;
    int height = H;
    // In order to measure pure decoding speed, we convert the entire image to a greyscale array
    // up front, which is the same as the Y channel of the YUVLuminanceSource in the real app.
    luminances = new unsigned char[width * height];
    for (int y = 0; y < height; y++)
    {
        int offset = y * width;
        for (int x = 0; x < width; x++)
        {
            int r = d[offset * 3 + x * 3];
            int g = d[offset * 3 + x * 3 + 1];
            int b = d[offset * 3 + x * 3 + 2];
            if (r == g && g == b)
            {
                // Image is already greyscale, so pick any channel.
                luminances[offset + x] = (unsigned char)r;
            }
            else
            {
                // Calculate luminance cheaply, favoring green.
                luminances[offset + x] = (unsigned char)((r + g + g + b) >> 2);
            }
        }
    }
}*/
RGBLuminanceSource::RGBLuminanceSource(char* d, int W, int H):LuminanceSource(W,H)
{
    isRotated = false;
    _isQRcodeImage = true;
    __width = W;
    __height = H;
    int len = W * H;
    luminances = new char[len];
    //luminances = ArrayRef<char>(len);
    for(int i=0; i<len; i++)
    {
        luminances[i]= d[i];
    }
}
RGBLuminanceSource::RGBLuminanceSource(char* d, int W, int H,int maxGray,int minGary):LuminanceSource(W,H)
{
    isRotated = false;

    __width = W;
    __height = H;
    int len = W * H;
    luminances = new char[len];
    //luminances = ArrayRef<char> (len);
    int totalGray = 0;
    for(int i=0; i<len; i++)
    {
        luminances[i]= d[i];
        totalGray += (unsigned char)(d[i] < 0 ? d[i] + 256 : d[i]);
    }

    int aveGray = totalGray/len;
    _isQRcodeImage = (aveGray < maxGray) && (aveGray > minGary);

}
bool RGBLuminanceSource::isQRcodeImage()
{
    return _isQRcodeImage;
}
/*  RGBLuminanceSource::RGBLuminanceSource(unsigned char* d, int W, int H,bool Is8Bit){

       __width = W;
       __height = H;
       luminances = new char[W * H];
       Buffer.BlockCopy(d,0, luminances,0, W * H);
}*/
/*
        RGBLuminanceSource::RGBLuminanceSource(unsigned char[] d, int W, int H, bool Is8Bit,Rectangle Region){
    //ctor
}
        RGBLuminanceSource::RGBLuminanceSource(Bitmap d, int W, int H){
    //ctor
}*/

RGBLuminanceSource::~RGBLuminanceSource()
{
    //dtor
    //if(!_isQRcodeImage)
        delete[] luminances;
}

int RGBLuminanceSource::getHeight() const
{
    if (!isRotated)
        return __height;
    else
        return __width;
}
int RGBLuminanceSource::getWidth() const
{
    if (!isRotated)
        return __width;
    else
        return __height;
}
ArrayRef<char> RGBLuminanceSource::getRow(int y, ArrayRef<char> row)const
{
    int width = getWidth();
    if(!row)
    {
        row = ArrayRef<char>(width);
    }
    for (int i = 0; i < width; i++)
    {
        row[i] = luminances[y * width + i];
    }
    return row;
}

#if 0
char* RGBLuminanceSource::getRow(int y, char* row,int rowLength)
{
    if (isRotated == false)
    {
        int width = getWidth();
        if (row == NULL || rowLength < width)
        {
            row = new char[width];
        }
        for (int i = 0; i < width; i++)
            row[i] = luminances[y * width + i];
        //System.arraycopy(luminances, y * width, row, 0, width);
        return row;
    }
    else
    {
        int width = __width;
        int height = __height;
        if (row == NULL || rowLength < height)
        {
            row = new char[height];
        }
        for (int i = 0; i < height; i++)
            row[i] = luminances[i * width + y];
        //System.arraycopy(luminances, y * width, row, 0, width);
        return row;
    }
}
#endif
//#include <stdio.h>
ArrayRef<char> RGBLuminanceSource::getMatrix()const
{
#if 1
    int length = __width * __height;
    ArrayRef<char> matrix(length);
    //printf("length = %d \n",length);

    //memcpy((void *)&matrix,(void *)luminances,length);

    for(int i = 0;i<length;i++)
    {
        matrix[i] = luminances[i];
    }

    //printf("length2 = %d \n",length);
     //while(1);
    return matrix;
    #endif // 0
   //return luminances;
}
Ref<LuminanceSource> RGBLuminanceSource::crop(int left, int top, int width, int height)
{
    throw IllegalArgumentException("This luminance source does not support cropping.");
}
Ref<LuminanceSource> RGBLuminanceSource::rotateCounterClockwise()
{
    isRotated = true;
    return Ref<LuminanceSource>(this);
    //throw IllegalArgumentException("This luminance source does not support cropping.");

}
bool RGBLuminanceSource::isRotateSupported()
{
    return true;
}

}
