// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  AztecReader.cpp
 *  zxing2
 *
 *  Created by Lukas Stabe on 08/02/2012.
 *  Copyright 2012 zxing2 authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <zxing2/aztec/AztecReader.h>
#include <zxing2/aztec/detector/Detector.h>
#include <zxing2/common/DecoderResult.h>
#include <iostream>

using zxing2::Ref;
using zxing2::ArrayRef;
using zxing2::Result;
using zxing2::aztec::AztecReader;

// VC++
using zxing2::BinaryBitmap;
using zxing2::DecodeHints;

AztecReader::AztecReader() : decoder_() {
  // nothing
}

Ref<Result> AztecReader::decode(Ref<zxing2::BinaryBitmap> image) {
  Detector detector(image->getBlackMatrix());

  Ref<AztecDetectorResult> detectorResult(detector.detect());

  ArrayRef< Ref<ResultPoint> > points(detectorResult->getPoints());

  Ref<DecoderResult> decoderResult(decoder_.decode(detectorResult));

  Ref<Result> result(new Result(decoderResult->getText(),
                                decoderResult->getRawBytes(),
                                points,
                                BarcodeFormat::AZTEC));

  return result;
}

Ref<Result> AztecReader::decode(Ref<BinaryBitmap> image, DecodeHints) {
  //cout << "decoding with hints not supported for aztec" << "\n" << flush;
  return this->decode(image);
}

AztecReader::~AztecReader() {
  // nothing
}

zxing2::aztec::Decoder& AztecReader::getDecoder() {
  return decoder_;
}
