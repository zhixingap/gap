// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  GenericGF.cpp
 *  zxing
 *
 *  Created by Lukas Stabe on 13/02/2012.
 *  Copyright 2012 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include "GF256.h"
#include "GF256Poly.h"
#include "../IllegalArgumentException.h"

using zxing::GF256;
using zxing::GF256Poly;
using zxing::Ref;

Ref<GF256> GF256::QR_CODE_FIELD_256(new GF256(0x011D));
Ref<GF256> GF256::DATA_MATRIX_FIELD_256(new GF256(0x012D));


//static int INITIALIZATION_THRESHOLD = 0;

GF256::GF256(int primitive)
    : primitive_(primitive)
{

    expTable_.resize(256);
    logTable_.resize(256);
    expTable_.size();
    int x = 1;

    for (int i = 0; i < 256; i++)
    {
        expTable_[i] = x;
        x <<= 1; // x = x * 2; we're assuming the generator alpha is 2
        if (x >= 256)
        {
            x ^= primitive_;
            x &= 256-1;
        }
    }
    for (int i = 0; i < 256-1; i++)
    {
        logTable_[expTable_[i]] = i;
    }
    //logTable_[0] == 0 but this should never be used

    zero_ = Ref<GF256Poly>(new GF256Poly(Ref<GF256>(this), ArrayRef<int>(new Array<int>(1))));
    zero_->getCoefficients()[0] = 0;
    one_ = Ref<GF256Poly>(new GF256Poly(Ref<GF256>(this), ArrayRef<int>(new Array<int>(1))));
    one_->getCoefficients()[0] = 1;
    //}
}


/*
void GenericGF::checkInit() {
  if (!initialized_) {
    initialize();
  }
}
*/
Ref<GF256Poly> GF256::getZero()
{
    //checkInit();
    return zero_;
}

Ref<GF256Poly> GF256::getOne()
{
    //checkInit();
    return one_;
}

Ref<GF256Poly> GF256::buildMonomial(int degree, int coefficient)
{
//  checkInit();

    if (degree < 0)
    {
        throw IllegalArgumentException("Degree must be non-negative");
    }
    if (coefficient == 0)
    {
        return zero_;
    }
    ArrayRef<int> coefficients(new Array<int>(degree + 1));
    coefficients[0] = coefficient;

    //return new GenericGFPoly(this, coefficients);
    return Ref<GF256Poly>(new GF256Poly(Ref<GF256>(this), coefficients));
}

int GF256::addOrSubtract(int a, int b)
{
    return a ^ b;
}

int GF256::exp(int a)
{
    //checkInit();
    return expTable_[a];
}

int GF256::log(int a)
{
    //checkInit();
    if (a == 0)
    {
        throw IllegalArgumentException("cannot give log(0)");
    }
    return logTable_[a];
}

int GF256::inverse(int a)
{
    //checkInit();
    if (a == 0)
    {
        throw IllegalArgumentException("Cannot calculate the inverse of 0");
    }
    return expTable_[256 - logTable_[a] - 1];
}

int GF256::multiply(int a, int b)
{
    //checkInit();

    if (a == 0 || b == 0)
    {
        return 0;
    }
    if(a==1)
    {
        return b;
    }
    if(b==1)
    {
        return a;
    }

    return expTable_[(logTable_[a] + logTable_[b]) % (256 - 1)];
}

int GF256::getSize()
{
    return 256;
}
