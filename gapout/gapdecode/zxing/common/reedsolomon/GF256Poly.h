// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  GenericGFPoly.h
 *  zxing
 *
 *  Created by Lukas Stabe on 13/02/2012.
 *  Copyright 2012 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GENERICGFPOLY_Hz
#define GENERICGFPOLY_Hz

#include <vector>
#include "../Array.h"
#include "../Counted.h"

namespace zxing
{
class GF256;

class GF256Poly : public Counted
{
private:
    Ref<GF256> field_;
    ArrayRef<int> coefficients_;

public:
    GF256Poly(Ref<GF256> field, ArrayRef<int> coefficients);
    ArrayRef<int> getCoefficients();
    int getDegree();
    bool isZero();
    int getCoefficient(int degree);
    int evaluateAt(int a);
    Ref<GF256Poly> addOrSubtract(Ref<GF256Poly> other);
    Ref<GF256Poly> multiply(Ref<GF256Poly> other);
    Ref<GF256Poly> multiply(int scalar);
    Ref<GF256Poly> multiplyByMonomial(int degree, int coefficient);
    std::vector<Ref<GF256Poly> > divide(Ref<GF256Poly> other);

    //#warning todo: add print method
};
}

#endif //GENERICGFPOLY_H
