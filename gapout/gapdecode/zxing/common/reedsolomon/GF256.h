// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  GenericGF.h
 *  zxing
 *
 *  Created by Lukas Stabe on 13/02/2012.
 *  Copyright 2012 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GENERICGF_Hz
#define GENERICGF_Hz

#include <vector>
#include "../Counted.h"

namespace zxing
{
class GF256Poly;

class GF256 : public Counted
{

private:
    std::vector<int> expTable_;
    std::vector<int> logTable_;
    Ref<GF256Poly> zero_;
    Ref<GF256Poly> one_;
    int primitive_;


public:

    static Ref<GF256> QR_CODE_FIELD_256;
    static Ref<GF256> DATA_MATRIX_FIELD_256;

    GF256(int primitive);

    Ref<GF256Poly> getZero();
    Ref<GF256Poly> getOne();
    int getSize();
    Ref<GF256Poly> buildMonomial(int degree, int coefficient);

    static int addOrSubtract(int a, int b);
    int exp(int a);
    int log(int a);
    int inverse(int a);
    int multiply(int a, int b);

    bool operator==(GF256 other)
    {
        return (other.getSize() == 256 &&
                other.primitive_ == this->primitive_);
    }

    //#warning todo: add print method

};
}

#endif //GENERICGF_H

