#include <iostream>
#include <string>

#include "zxing/qrcode/QRCodeReader.h"
#include "zxing/RGBLuminanceSource.h"
#include "zxing/LuminanceSource.h"
#include "zxing/common/HybridBinarizer.h"
#include "zxing/Result.h"
#include "zxing/common/Str.h"
#include "zxing/ReaderException.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <dirent.h>
#include <errno.h>

using namespace std;
using namespace zxing;
using namespace zxing::qrcode;

QRCodeReader _qrcode;
void *retryThread1(void *param)
{


    for(int i=0; i<0; i++)
    {
        sleep(2);
        char c1[12];
        sprintf(c1,"%d",i);
        string str = "/root/res1";
        //str +=c1;
        str += ".bmp";

        IplImage* img2 = cvLoadImage(str.c_str());
        //int channels = img2->nChannels;
        IplImage* imgGray = cvCreateImage(cvSize(img2->width,img2->height),IPL_DEPTH_8U,1);
        //channels = imgGray->nChannels;
        cvCvtColor(img2,imgGray,CV_RGB2GRAY);

        int w = imgGray->width;
        int h = imgGray->height;


        struct timeval t1,t2;//,t3;
        gettimeofday(&t1,NULL);




        Ref<RGBLuminanceSource> source = Ref<RGBLuminanceSource>(new RGBLuminanceSource(imgGray->imageData,w,h,180,80));

        if(!source->isQRcodeImage())
        {
            continue;
        }
        Ref<HybridBinarizer> hy = Ref<HybridBinarizer>(new HybridBinarizer(source));

        Ref<BinaryBitmap> bitmap = Ref<BinaryBitmap>(new BinaryBitmap(hy));



        DecodeHints hints;
        Ref<Result> result;

        try
        {
            result = _qrcode.decode(bitmap,hints);

        }
        catch(Exception e)
        {
            printf("%s\n",e.what());

        }
        gettimeofday(&t2,NULL);
        float total = (t2.tv_sec-t1.tv_sec)*1000 + (t2.tv_usec-t1.tv_usec)/1000;
        printf("%f \n ",total);

        cvReleaseImage(&img2);
        cvReleaseImage(&imgGray);

        //printf("%s\n",result->getText()->getText().c_str());
        printf("thread :%d\n",i);

    }

    return NULL;
}
int main()
{
    //pthread_t retry1,retry2;
    //pthread_create(&retry1,NULL,retryThread1,NULL);
//while(1){
    string path("/home/decodetemp");
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(path.c_str())) == NULL)
    {
        cout << "Error(" << errno << ") opening " << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL)
    {

        string str(dirp->d_name);
        if(str.length()<3) continue;
        string tmp = path +"/"+str;
        //cout << str << endl;
        IplImage* img2 = cvLoadImage(tmp.c_str());

        cout<<tmp<<endl;


        // if(!str.compare("error2.bmp"))
        //     cvSaveImage("/root/240.bmp",dst);


        IplImage* imgGray = cvCreateImage(cvSize(img2->width,img2->height),IPL_DEPTH_8U,1);

        cvCvtColor(img2,imgGray,CV_RGB2GRAY);

        //IplImage* dst = cvCreateImage(cvSize(imgGray->width/2,imgGray->height/2),IPL_DEPTH_8U,1);
        //cvResize(imgGray,dst,CV_INTER_AREA);//CV_INTER_NN,CV_INTER_CUBIC


        int w = imgGray->width;
        int h = imgGray->height;


        struct timeval t1,t2;//,t3;
        gettimeofday(&t1,NULL);



        Ref<RGBLuminanceSource> source = Ref<RGBLuminanceSource>(new RGBLuminanceSource(imgGray->imageData,w,h,254,0));

        if(!source->isQRcodeImage())
        {
            continue;
        }

// source is same with C#



        Ref<HybridBinarizer> hy = Ref<HybridBinarizer>(new HybridBinarizer(source));
//hy->getBlackMatrix();


        Ref<BinaryBitmap> bitmap = Ref<BinaryBitmap>(new BinaryBitmap(hy));



        //DecodeHints hints;
        //hints.setTryHarder(true);

        Ref<Result> result;

        try
        {
            result = _qrcode.decode(bitmap,NULL);
            gettimeofday(&t2,NULL);
            float total = (t2.tv_sec-t1.tv_sec)*1000 + (t2.tv_usec-t1.tv_usec)/1000;
            printf("%f \n",total);
            if(result->getText()->getText().length()>20){
                //cout<<result->getText()->getText()<<endl;
            }


        }
        catch(Exception e)
        {
            //printf("%s\n",e.what());

        }

        cvReleaseImage(&img2);
         //cvReleaseImage(&dst);
        cvReleaseImage(&imgGray);

    }
    closedir(dp);

    //cout << "end" << endl;
//}

    return 0;
}
