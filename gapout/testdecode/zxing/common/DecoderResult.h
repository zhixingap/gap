#ifndef __DECODER_RESULT_Hz__
#define __DECODER_RESULT_Hz__

/*
 *  DecoderResult.h
 *  zxing
 *
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Counted.h"
#include "Array.h"
#include <string>
#include "Str.h"
#include "../qrcode/ErrorCorrectionLevel.h"

namespace zxing
{

class DecoderResult : public Counted
{
private:
    ArrayRef<char> rawBytes_;
    Ref<String> text_;
    ArrayRef< ArrayRef<char> > byteSegments_;
    qrcode::ErrorCorrectionLevel* ecLevel_;

public:
    DecoderResult(ArrayRef<char> rawBytes,
                  Ref<String> text,
                  ArrayRef< ArrayRef<char> >& byteSegments,
                  qrcode::ErrorCorrectionLevel ecLevel);

    DecoderResult(ArrayRef<char> rawBytes, Ref<String> text);

    ArrayRef<char> getRawBytes();
    Ref<String> getText();
};

}

#endif // __DECODER_RESULT_H__
