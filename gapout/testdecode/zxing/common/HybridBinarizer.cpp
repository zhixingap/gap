// -*- mode:c++; tab-width:2; indent-tabs-mode:nil; c-basic-offset:2 -*-
/*
 *  HybridBinarizer.cpp
 *  zxing
 *
 *  Copyright 2010 ZXing authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HybridBinarizer.h"

#include "IllegalArgumentException.h"

using namespace std;
using namespace zxing;

namespace
{
const int BLOCK_SIZE_POWER = 3;
const int BLOCK_SIZE = 1 << BLOCK_SIZE_POWER; // ...0100...00
const int BLOCK_SIZE_MASK = BLOCK_SIZE - 1;   // ...0011...11
const int MINIMUM_DIMENSION = 40;//BLOCK_SIZE * 5;
}

HybridBinarizer::HybridBinarizer(Ref<LuminanceSource> source) :
    GlobalHistogramBinarizer(source), matrix_(NULL)
{
}

HybridBinarizer::~HybridBinarizer()
{
}


Ref<Binarizer>
HybridBinarizer::createBinarizer(Ref<LuminanceSource> source)
{
    return Ref<Binarizer> (new HybridBinarizer(source));
}


/**
 * Calculates the final BitMatrix once for all requests. This could be called once from the
 * constructor instead, but there are some advantages to doing it lazily, such as making
 * profiling easier, and not doing heavy lifting when callers don't expect it.
 */
Ref<BitMatrix> HybridBinarizer::getBlackMatrix()
{
    if (matrix_)
    {
        return matrix_;
    }
    LuminanceSource& source = *getLuminanceSource();

    if (source.getWidth() >= MINIMUM_DIMENSION && source.getHeight() >= MINIMUM_DIMENSION)
    {
        char* luminances = source.getMatrix();
        int width = source.getWidth();
        int height = source.getHeight();
        int subWidth = width >> 3;
        int subHeight = height >> 3;

        int** blackPoints =
            calculateBlackPoints(luminances, subWidth, subHeight, width);

        Ref<BitMatrix> newMatrix (new BitMatrix(width, height));
        calculateThresholdForBlock(luminances,
                                   subWidth,
                                   subHeight,
                                   width,
                                   height,
                                   blackPoints,
                                   newMatrix);
        matrix_ = newMatrix;

        // N.B.: these deletes are inadequate if anything between the new
        // and this point can throw.  As of this writing, it doesn't look
        // like they do.


        delete [] luminances;
        for(int i=0; i<subHeight; i++)
        {
            delete [] blackPoints[i];
        }
        delete[] blackPoints;

    }
    else
    {
        // If the image is too small, fall back to the global histogram approach.
        matrix_ = GlobalHistogramBinarizer::getBlackMatrix();
    }

    return matrix_;
}

namespace
{
inline int cap(int value, int min, int max)
{
    return value < min ? min : value > max ? max : value;
}
}

void
HybridBinarizer::calculateThresholdForBlock(char* luminances,
        int subWidth,
        int subHeight,
        int width,
        int height,
        int **blackPoints,
        Ref<BitMatrix> const& matrix)
{
    for (int y = 0; y < subHeight; y++)
    {
        for(int x=0; x<subWidth; x++)
        {

            int left = (x>1)?x:2;
            left = (left<(subWidth-2))?left:(subWidth-3);
            int top=y>1?y:2;
            top = (top<(subHeight-2))?top:(subHeight-3);
            int sum = 0;
            for (int z = -2; z <= 2; z++)
            {
                int *blackRow = blackPoints[top + z];
                sum += blackRow[left - 2];
                sum += blackRow[left - 1];
                sum += blackRow[left];
                sum += blackRow[left + 1];
                sum += blackRow[left + 2];
            }
            int average = sum / 25;
            //cout<<x<<","<<y<<","<<average<<","<<width<<endl;
            thresholdBlock(luminances, x<<3, y<<3, average, width, matrix);
        }
    }
}

void HybridBinarizer::thresholdBlock(char* luminances,
                                     int xoffset,
                                     int yoffset,
                                     int threshold,
                                     int stride,
                                     Ref<BitMatrix> const& matrix)
{
//cout<<xoffset<<","<<yoffset<<","<<threshold<<","<<stride<<endl;


    for (int y = 0;
            y < 8;
            y++)
    {
        int  offset = (yoffset+y) * stride + xoffset;

        for (int x = 0; x < 8; x++)
        {
            int pixel = luminances[offset + x] & 0xff;
            //cout<<offset<<","<<x<<","<<pixel<<","<<threshold<<endl;
            if (pixel < threshold)
            {
                matrix->set(xoffset + x, yoffset + y);
                //cout<<xoffset<<","<<x<<","<<yoffset<<","<<y<<endl;
                //cout<<matrix->getBits()[0]<<endl;
            }
            else
            {
                //cout<<"no"<<endl;
            }
        }
    }
}

namespace
{
inline int getBlackPointFromNeighbors(int* blackPoints, int subWidth, int x, int y)
{
    return (blackPoints[(y-1)*subWidth+x] +
            2*blackPoints[y*subWidth+x-1] +
            blackPoints[(y-1)*subWidth+x-1]) >> 2;
}
}


int** HybridBinarizer::calculateBlackPoints(char* luminances,
        int subWidth,
        int subHeight,
        int stride)
{


    int **blackPoints = new int*[subHeight];
    for(int i=0; i<subHeight; i++)
    {
        blackPoints[i]=new int[subWidth];
    }
    for (int y = 0; y < subHeight; y++)
    {

        for(int x=0; x<subWidth; x++)
        {
            int sum=0;
            int min=255;
            int max=0;

            for (int yy = 0; yy < 8; yy++)
            {
                int offset=((y<<3)+yy)*stride +(x<<3);

                for (int xx = 0; xx < 8; xx++)
                {
                    int pixel = luminances[offset + xx] & 0xff;
                    sum += pixel;
                    // still looking for good contrast
                    if (pixel < min)
                    {
                        min = pixel;
                    }
                    if (pixel > max)
                    {
                        max = pixel;
                    }
                }
            }

            int average = (max-min)>24?(sum>>6):(min>>1);

            blackPoints[y][x] = average;
        }
    }
    return blackPoints;
}

